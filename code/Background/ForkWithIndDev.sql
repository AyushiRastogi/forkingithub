drop table fsoc.forkWithIndDev_2008;
drop table fsoc.forkWithIndDev_2009;
drop table fsoc.forkWithIndDev_2010;
drop table fsoc.forkWithIndDev_2011;
drop table fsoc.forkWithIndDev_2012;

create table fsoc.forkWithIndDev_2008
select mf_id
,count(distinct pf_id) forkWithIndDev
from fsoc.forkingStatistics
group by mf_id;

alter table fsoc.forkwithinddev_2008
add index mf_id(mf_id);

create table fsoc.forkWithIndDev_2009
select mf_id
,count(distinct pf_id) forkWithIndDev
from fsoc.forkingStatistics
group by mf_id;

alter table fsoc.forkwithinddev_2009
add index mf_id(mf_id);

create table fsoc.forkWithIndDev_2010
select mf_id
,count(distinct pf_id) forkWithIndDev
from fsoc.forkingStatistics
group by mf_id;

alter table fsoc.forkwithinddev_2010
add index mf_id(mf_id);

create table fsoc.forkWithIndDev_2011
select mf_id
,count(distinct pf_id) forkWithIndDev
from fsoc.forkingStatistics
group by mf_id;

alter table fsoc.forkwithinddev_2011
add index mf_id(mf_id);

create table fsoc.forkWithIndDev_2012
select mf_id
,count(distinct pf_id) forkWithIndDev
from fsoc.forkingStatistics
group by mf_id;

alter table fsoc.forkwithinddev_2012
add index mf_id(mf_id);