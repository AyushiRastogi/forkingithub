
alter table github.followers
add index user_id(user_id);

drop table if exists fsoc.follower_hierarchy;
create table fsoc.follower_hierarchy
select a.user_id
,a.follower_id
,b.follower_id followers_follower_id
from github.followers a
left join github.followers b
on a.follower_id=b.user_id;

alter table fsoc.follower_hierarchy
add index uf(user_id,follower_id);

create table fsoc.followers_follower_count
select user_id
,follower_id
,count(followers_follower_id) followers_follower_count
from fsoc.follower_hierarchy
group by user_id,follower_id;

alter table fsoc.followers_follower_count
add index uf(user_id,follower_id);

create table fsoc.followers_count
select user_id,sum(followers_follower_count) followers_count
from fsoc.followers_follower_count
group by user_id;

alter table fsoc.followers_count
add index user_id(user_id);
