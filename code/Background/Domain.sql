create table fsoc.projectDomain
select id 
,if(lower(description) like '% application%' or lower(description) like '% app%' or lower(description) like '% apps %' or lower(description) like '% tool%' or lower(description) like '% add-ons%' or lower(description) like '% generator%' or lower(description) like '%service%' or lower(description) like '%development%','Application' 
,if(lower(description) like '%database%' or lower(description) like '%mvc%' or lower(description) like '%server%' or lower(description) like '%client%' or lower(description) like '%network%','Database' 
,if(lower(description) like '%compiler%' or lower(description) like '%parser%' or lower(description) like '%interpreter%' or lower(description) like '%bootstrap%' or lower(description) like '%binding%' or lower(description) like '%build%','Compiler' 
,if(lower(description) like '%platform%' or lower(description) like '%client%' or lower(description) like '%server%' or lower(description) like '% port%','MiddleWare' 
,if(lower(description) like '%library%' or lower(description) like '%libraries%' or lower(description) like '% api%' or lower(description) like '%wrapper%' or lower(description) like '%package%' or lower(description) like '%driver%' or lower(description) like '%wrapper%' or lower(description) like '%bindings%' or lower(description) like '%lightweight%','library' 
,if(lower(description) like '%framework%' or lower(description) like'%environment%' or lower(description) like '% sdk%' or lower(description) like '%transforming%' or lower(description) like '%plugin%' or lower(description) like '%interface%' or lower(description) like '%template%' or lower(description) like '%model%','Framework','Others')))))) Domain 
from  github.projects;

alter table fsoc.projectDomain
add index id(id);