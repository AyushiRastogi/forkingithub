create table fsoc.watchers_count
select repo_id
,count(user_id) watchers_count
from github.watchers
group by repo_id;

alter table fsoc.watchers_count
add index repo_id(repo_id);

