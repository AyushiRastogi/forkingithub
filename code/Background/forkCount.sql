create table fsoc.fork_Count_2008
select mf_id
,count(distinct pf_id) fork_Count
from fsoc.presf_2008
group by mf_id;

create table fsoc.fork_Count_2009
select mf_id
,count(distinct pf_id) fork_Count
from fsoc.presf_2009
group by mf_id;

create table fsoc.fork_Count_2010
select mf_id
,count(distinct pf_id) fork_Count
from fsoc.presf_2010
group by mf_id;

create table fsoc.fork_Count_2011
select mf_id
,count(distinct pf_id) fork_Count
from fsoc.presf_2011
group by mf_id;

create table fsoc.fork_Count_2012
select mf_id
,count(distinct pf_id) fork_Count
from fsoc.presf_2012
group by mf_id;

alter table fsoc.fork_Count_2008
add index mf_id(mf_id);
alter table fsoc.fork_Count_2009
add index mf_id(mf_id);
alter table fsoc.fork_Count_2010
add index mf_id(mf_id);
alter table fsoc.fork_Count_2011
add index mf_id(mf_id);
alter table fsoc.fork_Count_2012
add index mf_id(mf_id);