drop table if exists  fsoc.forkingIntentions_2008;
create table fsoc.forkingIntentions_2008
select fs.mf_id
,mf_forkCount  mf_forkWithIndDev
,mf_domain
,mf_watchersCount
,mf_followersCount
,mf_language
,mf_authorCount
,mf_commitCount
,timestampdiff(day,mf_createdAt,'2014-01-02')/365 projectAge
,pf_id
,mf_age/365 mf_age
,pf_watchersCount
,pf_followersCount
,pf_language
,pf_authorCount
,pf_commitCount
,SignificantFork significantFork
,Less less
from fsoc.forkingStatistics_2008 fs;


drop table if exists  fsoc.forkingIntentions_2009;
create table fsoc.forkingIntentions_2009
select fs.mf_id
,mf_forkCount  mf_forkWithIndDev
,mf_domain
,mf_watchersCount
,mf_followersCount
,mf_language
,mf_authorCount
,mf_commitCount
,timestampdiff(day,mf_createdAt,'2014-01-02')/365 projectAge
,pf_id
,mf_age/365 mf_age
,pf_watchersCount
,pf_followersCount
,pf_language
,pf_authorCount
,pf_commitCount
,SignificantFork significantFork
,Less less
from fsoc.forkingStatistics_2009 fs;


drop table if exists  fsoc.forkingIntentions_2010;
create table fsoc.forkingIntentions_2010
select fs.mf_id
,mf_forkCount  mf_forkWithIndDev
,mf_domain
,mf_watchersCount
,mf_followersCount
,mf_language
,mf_authorCount
,mf_commitCount
,timestampdiff(day,mf_createdAt,'2014-01-02')/365 projectAge
,pf_id
,mf_age/365 mf_age
,pf_watchersCount
,pf_followersCount
,pf_language
,pf_authorCount
,pf_commitCount
,SignificantFork significantFork
,Less less
from fsoc.forkingStatistics_2010 fs;

drop table if exists  fsoc.forkingIntentions_2011;
create table fsoc.forkingIntentions_2011
select fs.mf_id
,mf_forkCount mf_forkWithIndDev
,mf_domain
,mf_watchersCount
,mf_followersCount
,mf_language
,mf_authorCount
,mf_commitCount
,timestampdiff(day,mf_createdAt,'2014-01-02')/365 projectAge
,pf_id
,mf_age/365 mf_age
,pf_watchersCount
,pf_followersCount
,pf_language
,pf_authorCount
,pf_commitCount
,SignificantFork significantFork
,Less less
from fsoc.forkingStatistics_2011 fs;

drop table if exists  fsoc.forkingIntentions_2012;
create table fsoc.forkingIntentions_2012
select fs.mf_id
,mf_forkCount  mf_forkWithIndDev
,mf_domain
,mf_watchersCount
,mf_followersCount
,mf_language
,mf_authorCount
,mf_commitCount
,timestampdiff(day,mf_createdAt,'2014-01-02')/365 projectAge
,pf_id
,mf_age/365 mf_age
,pf_watchersCount
,pf_followersCount
,pf_language
,pf_authorCount
,pf_commitCount
,SignificantFork significantFork
,Less less
from fsoc.forkingStatistics_2012 fs;