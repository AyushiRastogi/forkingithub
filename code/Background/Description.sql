create table fsoc.mf_description
select mf_id
,description
from fsoc.presf_mf_2008 mf
left join github.projects prj
on prj.id=mf.mf_id
union
select mf_id
,description
from fsoc.presf_mf_2008 mf
left join github.projects prj
on prj.id=mf.mf_id
union
select mf_id
,description
from fsoc.presf_mf_2009 mf
left join github.projects prj
on prj.id=mf.mf_id
union
select mf_id
,description
from fsoc.presf_mf_2010 mf
left join github.projects prj
on prj.id=mf.mf_id
union
select mf_id
,description
from fsoc.presf_mf_2011 mf
left join github.projects prj
on prj.id=mf.mf_id
union
select mf_id
,description
from fsoc.presf_mf_2012 mf
left join github.projects prj
on prj.id=mf.mf_id;

select *
from fsoc.mf_description
where length(description);