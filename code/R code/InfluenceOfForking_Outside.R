library(RODBC)
year=c(2008,2009,2010,2011,2012)
for (quarter in 1:4)
{
  AllContributionBefore =data.frame(matrix(vector(), 0, 5, dimnames=list(c(),
                                                                         c("mf_id","pf_id","year","month","commit_count"))),
                                    stringsAsFactors=F)
  AllContributionAfter = data.frame(matrix(vector(), 0, 5, dimnames=list(c(),
                                                                         c("mf_id","pf_id","year","month","commit_count"))),
                                    stringsAsFactors=F)
  for (yr in 1:5)
  {
    db<-odbcConnect("forksustainabilityofcommunity")
    ContributionBefore<-sqlQuery(db,
                                 paste("select * from fsoc.before_CommitPatternQ",quarter,"_Outside_",year[yr],sep=''))
    close(db)
    AllContributionBefore<-rbind(AllContributionBefore,ContributionBefore)
    
    db<-odbcConnect("forksustainabilityofcommunity")
    ContributionAfter<-sqlQuery(db,
                                paste("select * from fsoc.after_CommitPatternQ",quarter,"_Outside_",year[yr],sep=''))
    close(db)  
    AllContributionAfter<-rbind(AllContributionAfter,ContributionAfter)
    
    L=c(unique(ContributionBefore["pf_id"]))
    L$p_value=rep(0,nrow(unique(ContributionBefore["pf_id"])))
    for (i in 1:nrow(unique(ContributionBefore["pf_id"])))
    {
      L$p_value[i]<-wilcox.test(ContributionAfter[ContributionAfter$pf_id==L$pf_id[i],"commit_count"],
                                ContributionBefore[ContributionBefore$pf_id==L$pf_id[i],"commit_count"],
                                alternative="less",exact=F)$p.value
    }
    L$pf_id[L$p_value<=0.05]
    
    
    G=c(unique(ContributionBefore["pf_id"]))
    G$p_value=rep(0,nrow(unique(ContributionBefore["pf_id"])))
    for (i in 1:nrow(unique(ContributionBefore["pf_id"])))
    {
      G$p_value[i]<-wilcox.test(ContributionAfter[ContributionAfter$pf_id==G$pf_id[i],"commit_count"],
                                ContributionBefore[ContributionBefore$pf_id==G$pf_id[i],"commit_count"],
                                alternative="greater",exact=F)$p.value
    }
    G$pf_id[G$p_value<=0.05]
    
    E=c(unique(ContributionBefore["pf_id"]))
    E$p_value=rep(0,nrow(unique(ContributionBefore["pf_id"])))
    for (i in 1:nrow(unique(ContributionBefore["pf_id"])))
    {
      E$p_value[i]<-wilcox.test(ContributionAfter[ContributionAfter$pf_id==E$pf_id[i],"commit_count"],
                                ContributionBefore[ContributionBefore$pf_id==E$pf_id[i],"commit_count"],exact=F)$p.value
    }
    E$pf_id[E$p_value>0.1]
    
    print(c(Quarter=quarter,Year=year[yr],less=length(L$pf_id[L$p_value<=0.05]),
            greater=length(G$pf_id[G$p_value<=0.05]),
            equal=length(E$pf_id[E$p_value>0.1])
            ,total=nrow(unique(ContributionBefore["pf_id"]))))
    
  }
  All_L=c(unique(AllContributionBefore["pf_id"]))
  All_L$p_value=rep(0,nrow(unique(AllContributionBefore["pf_id"])))
  for (i in 1:nrow(unique(AllContributionBefore["pf_id"])))
  {
    All_L$p_value[i]<-wilcox.test(AllContributionAfter[AllContributionAfter$pf_id==All_L$pf_id[i],"commit_count"],
                                  AllContributionBefore[AllContributionBefore$pf_id==All_L$pf_id[i],"commit_count"],
                                  alternative="less",exact=F)$p.value
  }
  All_L$pf_id[All_L$p_value<=0.05]
  
  
  All_G=c(unique(AllContributionBefore["pf_id"]))
  All_G$p_value=rep(0,nrow(unique(AllContributionBefore["pf_id"])))
  for (i in 1:nrow(unique(AllContributionBefore["pf_id"])))
  {
    All_G$p_value[i]<-wilcox.test(AllContributionAfter[AllContributionAfter$pf_id==All_G$pf_id[i],"commit_count"],
                                  AllContributionBefore[AllContributionBefore$pf_id==All_G$pf_id[i],"commit_count"],
                                  alternative="greater",exact=F)$p.value
  }
  All_G$pf_id[All_G$p_value<=0.05]
  
  All_E=c(unique(AllContributionBefore["pf_id"]))
  All_E$p_value=rep(0,nrow(unique(AllContributionBefore["pf_id"])))
  for (i in 1:nrow(unique(AllContributionBefore["pf_id"])))
  {
    All_E$p_value[i]<-wilcox.test(AllContributionAfter[AllContributionAfter$pf_id==All_E$pf_id[i],"commit_count"],
                                  AllContributionBefore[AllContributionBefore$pf_id==All_E$pf_id[i],"commit_count"],exact=F)$p.value
  }
  All_E$pf_id[All_E$p_value>0.1]
  
  print(c(less=length(All_L$pf_id[All_L$p_value<=0.05]),
          greater=length(All_G$pf_id[All_G$p_value<=0.05]),
          equal=length(All_E$pf_id[All_E$p_value>0.1])
          ,total=nrow(unique(AllContributionBefore["pf_id"]))))
}

PF_id_less_outside=data.frame(pf_id=All_L$pf_id[All_L$p_value<=0.05])
PF_id_greater_outside=data.frame(pf_id=All_G$pf_id[All_G$p_value<=0.05])
PF_id_equal_outside=data.frame(pf_id=All_E$pf_id[All_E$p_value>0.1])
db<-odbcConnect("fsoc")
sqlSave(db,PF_id_less_outside,safer=FALSE)
close(db)
db<-odbcConnect("fsoc")
sqlSave(db,PF_id_greater_outside,safer=FALSE)
close(db)
db<-odbcConnect("fsoc")
sqlSave(db,PF_id_equal_outside,safer=FALSE)
close(db)