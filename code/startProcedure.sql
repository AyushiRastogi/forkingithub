# Start Point
delimiter $$
drop procedure if exists fsoc.startProcedure$$
create procedure fsoc.startProcedure()
begin
declare yr int;
declare quarter int;
declare cur cursor for
select year
from fsoc.year
where year between 2008 and 2012
order by year;
open cur;
get_year:loop
fetch cur into yr;

# Cluster projects into year
# Eliminate deleted projects
#call pre_MF(yr);
#call pre_MF_indexes(yr);

# Add index to fasten query in commits table
#alter table github.commits
#add index id_createdAt(id,created_at);

# Add index to watchers table
#alter table github.watchers
#add index repo_id(repo_id);

## Classify projects on development history
## Identify projects developed completely within GitHub
## Identify projects where commits are made after project creation on GitHub
##call MFCreatedBeforeStartDate(yr);
##call MFCreatedBeforeStartDate_indexes(yr);
## Eliminate projects which are mirror or clone of other projects
##call MFMirrorClone(yr);
##call MFMirrorClone_indexes(yr);

# Identify projects with PF
#call prePF(yr);
#call prePF_indexes(yr);
# Identify projects with SF
#call preSF(yr);
#call preSF_indexes(yr);
#call preSFTimestamped(yr);
#call mf_pf_presfLanguage(yr);
#call mf_pf_presfOwner(yr);
#call mf_pf_presfWatchersCount(yr);
#call mf_pf_presfFollowersCount(yr);
#call mf_pf_presfDomain(yr);

#call mf_pf_presfforkCount(yr);


# Type of Forks on Pull requests
#call preForkEndogenousPF(yr);
#call preForkEndogenousPF_indexes(yr);
#call preForkEndogenousSF(yr);
#call preForkEndogenousSF_indexes(yr);
#call preForkExogenous(yr);
#call preForkExogenous_indexes(yr);
#call preForkMFIntra(yr);
#call preForkMFIntra_indexes(yr);
#call preForkPFIntra(yr);
#call preForkPFIntra_indexes(yr);

# Categorise revisited
#call preForkMerged(yr);
#call preForkMerged_indexes(yr);
#call preForkExogenousNotMerged(yr);
#call preForkExogenousNotMerged_indexes(yr);
#call preForkPFIntraNotMerged(yr);
#call preForkPFIntraNotMerged_indexes(yr);
#call preForkType(yr);
#call preForkType_indexes(yr);

# Forks with no PR
#call preForkPFWithNoPR(yr);
#call preForkPFWithNoPR_indexes(yr);
#call preForkSFWithNoPR(yr);
#call preForkSFWithNoPR_indexes(yr);

# Commit History of Independently developed PF
#call CommitHistoryOfPF_IndDevPF(yr);
#call CommitHistoryOfPF_IndDevPF_indexes(yr);
#call MF_PF_WithIndependentDevelopment(yr);
#call MF_PF_WithIndependentDevelopment_indexes(yr);

# Identify Dormant forks
#call preforkPFDormant(yr);
#call preforkPFDormant_indexes(yr);

# Fork Types
#call ForkType(yr);
#call ForkType_indexes(yr);

# Forks with Exogenous and PF Intra Pull requests
#call MF_PF_WithExoIntraForks(yr);
#call MF_PF_WithExoIntraForks_indexes(yr);

# All forks
#call MF_PF_WithForks(yr);
#call MF_PF_WithForks_indexes(yr);

# Commit History
#call CommitHistory(yr);
#call CommitHistory_indexes(yr);

# Pull request History of Exogenous and PF Intra Forks
#call projectsWithForksOnPR(yr);
#call projectsWithForksOnPR_indexes(yr);
#call projectsWithPullRequestHistory(yr);
#call projectsWithPullRequestHistory_indexes(yr);

# Unique MF and PF in preSF
#call mf_pf_presf(yr);
#call mf_pf_presf_indexes(yr);

# Issue History
# Issues
#call mf_pf_issues(yr);
#call mf_pf_issues_indexes(yr);
# Issue Events
#call mf_pf_issueEvents(yr);
#call mf_pf_issueEvents_indexes(yr);
# Issue Comments
#call mf_pf_issueComments(yr);
#call mf_pf_issueComments_indexes(yr);

# Watcher History
#call mf_pf_watchers(yr);
#call mf_pf_watchers_indexes(yr);

## Contributor History
# Project Member History
#call mf_pf_ProjectMembers(yr);
#call mf_pf_ProjectMembers_indexes(yr);

# Download History
# Time stasis

##########################################################
# Analysis
#call mf_pf_CommitHistoryStatistics(yr);
#call mf_pf_CommitHistoryStatistics_indexes(yr);
#call CommitHistoryStatistics(yr);
#call CommitHistoryStatistics_indexes(yr);
call IdsWithSignificantForks(yr);
call IdsWithSignificantForks_indexes(yr);
set quarter=1;
qrtr:while(quarter<=4) do
call IdsWithSigDevHist(yr,quarter);
call IdsWithSigDevHist_indexes(yr,quarter);
call before_after_CommitPattern(yr,quarter);
call before_after_CommitPattern_indexes(yr,quarter);
call before_after_SerializedCommitPattern(yr,quarter);
call before_after_SerializedCommitPattern_indexes(yr,quarter);
call before_after_SerializedTimeDiffCommitPattern(yr,quarter);
call before_after_SerializedTimeDiffCommitPattern_indexes(yr,quarter);
set quarter=quarter+1;
end while qrtr;


############################################################
## Outside
## Projects developed completly or partially outside GitHub
##call Outside_MFMirrorClone(yr);
##call Outside_MFMirrorClone_indexes(yr);
end loop get_year;
close cur;
end$$
delimiter ;

#call fsoc.startProcedure();

