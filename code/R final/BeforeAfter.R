library(RODBC)
year=c(2008,2009,2010,2011,2012)
AllData = data.frame(matrix(vector(), 0, 7, dimnames=list(c(), c("MF_id","bQ1","bQ2","bQ3","aQ1","aQ2","aQ3"))), stringsAsFactors=F)
ContributionInfluencedProjects = data.frame(matrix(vector(), 0, 7, dimnames=list(c(), c("MF_id","bQ1","bQ2","bQ3","aQ1","aQ2","aQ3"))), stringsAsFactors=F)
FrequencyInfluencedProjects = data.frame(matrix(vector(), 0, 7, dimnames=list(c(), c("MF_id","bQ1","bQ2","bQ3","aQ1","aQ2","aQ3"))), stringsAsFactors=F)
for (i in 1:5)
{
db<-odbcConnect("forksustainabilityofcommunity")
ContributionBefore<-sqlQuery(db,
                             paste("select * from forksustainabilityofcommunity.PF_MF_SerializedBeforeParticipationPattern_",year[i],sep=''))
close(db)

db<-odbcConnect("forksustainabilityofcommunity")
ContributionAfter<-sqlQuery(db,
                            paste("select * from forksustainabilityofcommunity.PF_MF_SerializedAfterParticipationPattern_",year[i],sep=''))
close(db)

db<-odbcConnect("forksustainabilityofcommunity")
firstDiffBefore<-sqlQuery(db,
                          paste("select * from forksustainabilityofcommunity.PF_MF_TimeContributionDiffBeforeParticipationPattern_",year[i],sep=''))
close(db)

db<-odbcConnect("forksustainabilityofcommunity")
firstDiffAfter<-sqlQuery(db,
                         paste("select * from forksustainabilityofcommunity.PF_MF_TimeContributionDiffAfterParticipationPattern_",year[i],sep=''))
close(db)

db<-odbcConnect("forksustainabilityofcommunity")
doubleDiffBefore<-sqlQuery(db,
                           paste("select * from forksustainabilityofcommunity.PF_MF_DoubleTimeContributionDiffBeforeParticipationPattern_",year[i],sep=''))
close(db)

db<-odbcConnect("forksustainabilityofcommunity")
doubleDiffAfter<-sqlQuery(db,
                          paste("select * from forksustainabilityofcommunity.PF_MF_DoubleTimeContributionDiffAfterParticipationPattern_",year[i],sep=''))
close(db)

ContributionStatsBefore<-aggregate(pull_request_count~MF_id,data=ContributionBefore, 
          FUN=function(x)c(Q=quantile(x,c(0.25,0.5,0.75))))

ContributionStatsAfter<-aggregate(pull_request_count~MF_id,data=ContributionAfter,
                                   FUN=function(x)c(Q=quantile(x,c(0.25,0.5,0.75))))

temp<-merge(ContributionStatsBefore, ContributionStatsAfter, by = "MF_id")
ContributionStats<-data.frame(temp[,1],temp[,2][,1:3],temp[,3][,1:3])
colnames(ContributionStats)<-c("MF_id","bQ1","bQ2","bQ3","aQ1","aQ2","aQ3")
AllData=rbind(AllData,ContributionStats)
ContributionAffected<-ContributionStats[ContributionStats[,3]>ContributionStats[,6] & ContributionStats[,4]>ContributionStats[,7],c(1,3,6,2,5,4,7)]
ContributionInfluencedProjects=rbind(ContributionInfluencedProjects,ContributionAffected)
##############################
FirstDiffStatsBefore<-aggregate(time_diff~MF_id,data=firstDiffBefore,
                           FUN=function(x)c(Q=quantile(x,c(0.25,0.5,0.75))))
FirstDiffStatsAfter<-aggregate(time_diff~MF_id,data=firstDiffAfter,
                          FUN=function(x)c(Q=quantile(x,c(0.25,0.5,0.75))))
temp<-merge(FirstDiffStatsBefore, FirstDiffStatsAfter, by = "MF_id")
FirstDiffStats<-data.frame(temp[,1],temp[,2][,1:3],temp[,3][,1:3])
colnames(FirstDiffStats)<-c("MF_id","bQ1","bQ2","bQ3","aQ1","aQ2","aQ3")
FrequencyAffected<-FirstDiffStats[FirstDiffStats[,3]<FirstDiffStats[,6] & FirstDiffStats[,4]<FirstDiffStats[,7] ,c(1,3,6,2,5,4,7)]
FrequencyInfluencedProjects=rbind(FrequencyInfluencedProjects,FrequencyAffected)
###################################
#DoubleDiffStatsBefore<-aggregate(doubleTimeDiff~MF_id,data=doubleDiffBefore,
 #                     FUN=function(x)c(Q=quantile(x,c(0.25,0.5,0.75))))
#DoubleDiffStatsAfter<-aggregate(doubleTimeDiff~MF_id,data=doubleDiffAfter,
 #                                FUN=function(x)c(Q=quantile(x,c(0.25,0.5,0.75))))

}

