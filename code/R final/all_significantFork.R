# Small
fit_all_s_01<-glm(significantFork~projectAge+
                    mf_authorCount+mf_followersCount+pf_followersCount+
                    mf_domain+
                    mf_watchersCount,
                  data=pc_all_small_trans,family=binomial(logit))
sum_fit_all_s_01<-summary(fit_all_s_01)


fit_all_s_02<-glm(significantFork~projectAge+
                    mf_authorCount+mf_followersCount*pf_followersCount+
                    mf_domain+
                    mf_watchersCount,
                  data=pc_all_small_trans,family=binomial(logit))
sum_fit_all_s_02<-summary(fit_all_s_02)

anv_fit_all_s_01_02<-anova(fit_all_s_01,fit_all_s_02,test="Chisq")
anv_fit_all_s_02<-anova(fit_all_s_02,test="Chisq")

###################################################################
# Medium
fit_all_m_01<-glm(significantFork~projectAge+
                    mf_authorCount+mf_followersCount+pf_followersCount+
                    mf_domain+
                    mf_watchersCount,
                  data=pc_all_medium_trans,family=binomial(logit))
sum_fit_all_m_01<-summary(fit_all_m_01)


fit_all_m_02<-glm(significantFork~projectAge+
                    mf_authorCount+mf_followersCount*pf_followersCount+
                    mf_domain+
                    mf_watchersCount,
                  data=pc_all_medium_trans,family=binomial(logit))
sum_fit_all_m_02<-summary(fit_all_m_02)
anv_fit_all_m_01_02<-anova(fit_all_m_01,fit_all_m_02,test="Chisq")
anv_fit_all_m_02<-anova(fit_all_m_02,test="Chisq")
##################################################################
# Large
fit_all_l_01<-glm(significantFork~projectAge+
                    mf_authorCount+mf_followersCount+pf_followersCount+
                    mf_domain+
                    mf_watchersCount,
                  data=pc_all_large_trans,family=binomial(logit))
sum_fit_all_l_01<-summary(fit_all_l_01)


fit_all_l_02<-glm(significantFork~projectAge+
                    mf_authorCount+mf_followersCount*pf_followersCount+
                    mf_domain+
                    mf_watchersCount,
                  data=pc_all_large_trans,family=binomial(logit))
sum_fit_all_l_02<-summary(fit_all_l_02)
anv_fit_all_l_01_02<-anova(fit_all_l_01,fit_all_l_02,test="Chisq")
anv_fit_all_l_02<-anova(fit_all_l_02,test="Chisq")
##################################################################
fit<-fit_all_s_02
fit<-fit_all_m_02
fit<-fit_all_l_01
vif(fit)
summary(fit)
anova(fit,test="Chisq")
1 - pc_allhisq(summary(fit)$deviance,summary(fit)$df.residual)

fit<-fit_all_s_02$fitted
hist(fit)

fitpreds = predict(fit,newdata=newdata,type="response")
fitpred = prediction(fitpreds,newdata$significantFork)
fitperf = performance(fitpred,"tpr","fpr")
plot(fitperf,col="green",lwd=2,main="ROC Curve for Logistic: significantFork")
abline(a=0,b=1,lwd=2,lty=2,col="gray")

