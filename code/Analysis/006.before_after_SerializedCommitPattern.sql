delimiter $$
drop procedure if exists fsoc.before_after_SerializedCommitPattern$$
create procedure fsoc.before_after_SerializedCommitPattern(in yr int,quarter int)
begin
set @counter:=0;
set @st=concat("drop table if exists fsoc.before_SerializedCommitPatternQ",quarter,"_Within_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.before_SerializedCommitPatternQ",quarter,"_Within_",yr," ",
"select @counter:=@counter+1 SNo ",
",mf_id ",
",pf_id ",
",year ",
",month ",
"from fsoc.before_CommitPatternQ",quarter,"_Within_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @counter:=0;
set @st=concat("drop table if exists fsoc.after_SerializedCommitPatternQ",quarter,"_Within_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.after_SerializedCommitPatternQ",quarter,"_Within_",yr," ",
"select @counter:=@counter+1 SNo ",
",mf_id ",
",pf_id ",
",year ",
",month ",
"from fsoc.after_CommitPatternQ",quarter,"_Within_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @counter:=0;
set @st=concat("drop table if exists fsoc.before_SerializedCommitPatternQ",quarter,"_Outside_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.before_SerializedCommitPatternQ",quarter,"_Outside_",yr," ",
"select @counter:=@counter+1 SNo ",
",mf_id ",
",pf_id ",
",year ",
",month ",
"from fsoc.before_CommitPatternQ",quarter,"_Outside_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @counter:=0;
set @st=concat("drop table if exists fsoc.after_SerializedCommitPatternQ",quarter,"_Outside_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.after_SerializedCommitPatternQ",quarter,"_Outside_",yr," ",
"select @counter:=@counter+1 SNo ",
",mf_id ",
",pf_id ",
",year ",
",month ",
"from fsoc.after_CommitPatternQ",quarter,"_Outside_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.before_after_SerializedCommitPattern_indexes$$
create procedure fsoc.before_after_SerializedCommitPattern_indexes(in yr int,quarter int)
begin
set @st=concat("alter table fsoc.before_SerializedCommitPatternQ",quarter,"_Within_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.before_SerializedCommitPatternQ",quarter,"_Within_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.after_SerializedCommitPatternQ",quarter,"_Within_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.after_SerializedCommitPatternQ",quarter,"_Within_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.before_SerializedCommitPatternQ",quarter,"_Outside_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.before_SerializedCommitPatternQ",quarter,"_Outside_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.after_SerializedCommitPatternQ",quarter,"_Outside_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.after_SerializedCommitPatternQ",quarter,"_Outside_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;

