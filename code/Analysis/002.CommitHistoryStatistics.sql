delimiter $$
drop procedure if exists fsoc.CommitHistoryStatistics$$
create procedure fsoc.CommitHistoryStatistics(in yr int)
begin
set @st=concat("drop table if exists fsoc.CommitHistoryStatistics_Within_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.CommitHistoryStatistics_Within_",yr," ",
"select distinct mf.mf_id ",
",mf_createdAt ",
",MF_language ",
",mf_owner ",
",mf.commit_count mf_commitCount ",
",mf.author_count mf_authorCount ",
",mf.committer_count mf_committerCount ",
",mf.DurationInDays mf_DurationInDays ",
",mf.min_commit_at mf_minCommitAt ",
",mf.max_commit_at mf_maxCommitAt ",
",pf.pf_id ",
",pf_createdAt ",
",PF_language ",
",pf_owner ",
",timestampdiff(day,mf_createdAt,PF_createdAt) MF_Age ",
",pf.commit_count pf_commitCount ",
",pf.author_count pf_authorCount ",
",pf.committer_count pf_committerCount ",
",pf.DurationInDays pf_DurationInDays ",
",pf.min_commit_at pf_minCommitAt ",
",pf.max_commit_at pf_maxCommitAt ",
"from fsoc.MF_CommitHistoryStatistics_",yr," mf ",
"left join fsoc.PF_CommitHistoryStatistics_",yr," pf ",
"on mf.mf_id=pf.mf_id ",
"left join fsoc.presf_",yr," sf ",
"on sf.mf_id=mf.mf_id ",
"and sf.PF_id=pf.pf_id ",
"where MF_createdAt<=mf.min_commit_at ",
"and pf_createdAt<=pf.min_commit_at");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("drop table if exists fsoc.CommitHistoryStatistics_Outside_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.CommitHistoryStatistics_Outside_",yr," ",
"select distinct mf.mf_id ",
",mf_createdAt ",
",MF_language ",
",mf_owner ",
",mf.commit_count mf_commitCount ",
",mf.author_count mf_authorCount ",
",mf.committer_count mf_committerCount ",
",mf.DurationInDays mf_DurationInDays ",
",mf.min_commit_at mf_minCommitAt ",
",mf.max_commit_at mf_maxCommitAt ",
",pf.pf_id ",
",pf_createdAt ",
",PF_language ",
",pf_owner ",
",timestampdiff(day,mf_createdAt,PF_createdAt) MF_Age ",
",pf.commit_count pf_commitCount ",
",pf.author_count pf_authorCount ",
",pf.committer_count pf_committerCount ",
",pf.DurationInDays pf_DurationInDays ",
",pf.min_commit_at pf_minCommitAt ",
",pf.max_commit_at pf_maxCommitAt ",
"from fsoc.MF_CommitHistoryStatistics_",yr," mf ",
"left join fsoc.PF_CommitHistoryStatistics_",yr," pf ",
"on mf.mf_id=pf.mf_id ",
"left join fsoc.presf_",yr," sf ",
"on sf.mf_id=mf.mf_id ",
"and sf.PF_id=pf.pf_id ",
"where MF_createdAt>mf.min_commit_at ",
"or pf_createdAt>pf.min_commit_at");

prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.CommitHistoryStatistics_indexes$$
create procedure fsoc.CommitHistoryStatistics_indexes(in yr int)
begin
set @st=concat("alter table fsoc.CommitHistoryStatistics_Within_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.CommitHistoryStatistics_Within_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.CommitHistoryStatistics_Outside_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.CommitHistoryStatistics_Outside_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;

