delimiter $$
drop procedure if exists fsoc.IdsWithSignificantForks$$
create procedure fsoc.IdsWithSignificantForks(in yr int)
begin
set @st=concat("drop table if exists fsoc.IdsWithSignificantForks_Within_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.IdsWithSignificantForks_Within_",yr," ",
"select mf_id ",
",pf_id ",
#",pf_minCommitAt+interval datediff(pf_maxCommitAt,pf_minCommitAt)/2 day break ",
",pf_minCommitAt break ",
"from fsoc.CommitHistoryStatistics_Within_",yr," ",
"where pf_commitCount>=100 ",
"and pf_DurationInDays>=90 ",
"and pf_committerCount>=2 ",
"and pf_authorCount>=4");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("drop table if exists fsoc.IdsWithSignificantForks_Outside_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.IdsWithSignificantForks_Outside_",yr," ",
"select mf_id ",
",pf_id ",
",pf_minCommitAt break ",
"from fsoc.CommitHistoryStatistics_Outside_",yr," ",
"where pf_commitCount>=100 ",
"and pf_DurationInDays>=90 ",
"and pf_committerCount>=2 ",
"and pf_authorCount>=4");

prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.IdsWithSignificantForks_indexes$$
create procedure fsoc.IdsWithSignificantForks_indexes(in yr int)
begin
set @st=concat("alter table fsoc.IdsWithSignificantForks_Within_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.IdsWithSignificantForks_Within_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.IdsWithSignificantForks_Outside_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.IdsWithSignificantForks_Outside_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;

