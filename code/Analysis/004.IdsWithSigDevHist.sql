delimiter $$
drop procedure if exists fsoc.IdsWithSigDevHist$$
create procedure fsoc.IdsWithSigDevHist(in yr int,quarter int)
begin
set @st=concat("drop table if exists fsoc.IdsWithSigDevHistQ",quarter,"_Within_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.IdsWithSigDevHistQ",quarter,"_Within_",yr," ",
"select stats.mf_id ",
",stats.pf_id ",
",break ",
"from fsoc.IdsWithSignificantForks_Within_",yr," ids ",
"left join fsoc.commithistoryStatistics_Within_",yr," stats ",
"on stats.mf_id=ids.mf_id ",
"and stats.pf_id=ids.pf_id ",
"where timestampdiff(day,mf_minCommitAt,break)>=",quarter*30," ",
"and timestampdiff(day,break,mf_maxCommitAt)>=",quarter*30);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("drop table if exists fsoc.IdsWithSigDevHistQ",quarter,"_Outside_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.IdsWithSigDevHistQ",quarter,"_Outside_",yr," ",
"select stats.mf_id ",
",stats.pf_id ",
",break ",
"from fsoc.IdsWithSignificantForks_Outside_",yr," ids ",
"left join fsoc.commithistoryStatistics_Outside_",yr," stats ",
"on stats.mf_id=ids.mf_id ",
"and stats.pf_id=ids.pf_id ",
"where timestampdiff(day,mf_minCommitAt,break)>=",quarter*30," ",
"and timestampdiff(day,break,mf_maxCommitAt)>=",quarter*30);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.IdsWithSigDevHist_indexes$$
create procedure fsoc.IdsWithSigDevHist_indexes(in yr int,quarter int)
begin
set @st=concat("alter table fsoc.IdsWithSigDevHistQ",quarter,"_Within_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.IdsWithSigDevHistQ",quarter,"_Within_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.IdsWithSigDevHistQ",quarter,"_Outside_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.IdsWithSigDevHistQ",quarter,"_Outside_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;

