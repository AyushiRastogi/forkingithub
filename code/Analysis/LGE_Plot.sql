# Equal
select *,'before' as type
from fsoc.before_commitpatternq4_outside_2008
where pf_id=1829056
union
select *,'after' as type
from fsoc.after_commitpatternq4_outside_2008
where pf_id=1829056;

# Greater
select *,'before' as type
from fsoc.before_commitpatternq4_outside_2012
where pf_id=965361
union
select *,'after' as type
from fsoc.after_commitpatternq4_outside_2012
where pf_id=965361;

# Less
select *,'before' as type
from fsoc.before_commitpatternq4_outside_2012
where pf_id=1770907
union
select *,'after' as type
from fsoc.after_commitpatternq4_outside_2012
where pf_id=1770907;

################################
# Equal
select *,'before' as type
from fsoc.before_commitpatternq4_within_2009
where pf_id=2160
union
select *,'after' as type
from fsoc.after_commitpatternq4_within_2009
where pf_id=2160;

# Greater
select *,'before' as type
from fsoc.before_commitpatternq4_within_2009
where pf_id=4504
union
select *,'after' as type
from fsoc.after_commitpatternq4_within_2009
where pf_id=4504;

# Less
select *,'before' as type
from fsoc.before_commitpatternq4_within_2008
where pf_id=812101
union
select *,'after' as type
from fsoc.after_commitpatternq4_within_2008
where pf_id=812101;


#17697, 2011
#690,2009

#############################
create table fsoc.less_within
select year,month,commit_count
from fsoc.before_commitpatternq4_within_2008
where pf_id=812101
union
select year,month,commit_count
from fsoc.after_commitpatternq4_within_2008
where pf_id=812101;

select ifnull(commit_count,0) commit_count
from fsoc.contribution_year_month ym
left join fsoc.less_within lw
on lw.year=ym.year
and lw.month=ym.month
where (ym.year>2009 or (ym.year=2009 and ym.month>=7)) and (ym.year<2013 or(ym.year=2013 and ym.month<=9))
order by ym.year,ym.month;

create table fsoc.greater_within
select year,month,commit_count
from fsoc.before_commitpatternq4_within_2009
where pf_id=4504
union
select year,month,commit_count
from fsoc.after_commitpatternq4_within_2009
where pf_id=4504;

select ifnull(commit_count,0) commit_count
from fsoc.contribution_year_month ym
left join fsoc.greater_within gw
on gw.year=ym.year
and gw.month=ym.month
where (ym.year>2009 or (ym.year=2009 and ym.month>=9)) and (ym.year<2013 or(ym.year=2013 and ym.month<=12))
order by ym.year,ym.month;

create table fsoc.equal_within
select year,month,commit_count
from fsoc.before_commitpatternq4_within_2009
where pf_id=2160
union
select year,month,commit_count
from fsoc.after_commitpatternq4_within_2009
where pf_id=2160;

select ifnull(commit_count,0) commit_count
from fsoc.contribution_year_month ym
left join fsoc.equal_within ew
on ew.year=ym.year
and ew.month=ym.month
where (ym.year>2010 or (ym.year=2010 and ym.month>=1)) and (ym.year<2013 or(ym.year=2013 and ym.month<=10))
order by ym.year,ym.month;


