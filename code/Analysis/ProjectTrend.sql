# Total project counts in the GHTorrent dataset
select count(id) project_count
,min(created_at) min_createdAt
,max(created_at) max_createdAt
from github.projects
where forked_from is null;

select count(id)
from github.users;
# Project count after eliminating deleted projects
# and clustering on year of creation
select '2008' as year
,count(mf_id) project_count
from fsoc.pre_mf_2008
union
select '2009' as year
,count(mf_id) project_count
from fsoc.pre_mf_2009
union
select '2010' as year
,count(mf_id) project_count
from fsoc.pre_mf_2010
union
select '2011' as year
,count(mf_id) project_count
from fsoc.pre_mf_2011
union
select '2012' as year
,count(mf_id) project_count
from fsoc.pre_mf_2012;

select '2008' as year
,count(distinct MF_id) sf_count
from fsoc.preSF_2008
union
select '2009' as year
,count(distinct MF_id) sf_count
from fsoc.preSF_2009
union
select '2010' as year
,count(distinct MF_id) sf_count
from fsoc.preSF_2010
union
select '2011' as year
,count(distinct MF_id) sf_count
from fsoc.preSF_2011
union
select '2012' as year
,count(distinct MF_id) sf_count
from fsoc.preSF_2012;

#  Projects with independently developed forks
select '2008' as year
,count(distinct mf_id) projectCount
from fsoc.pfwithForks_2008
union
select '2009' as year
,count(distinct mf_id) projectCount
from fsoc.pfwithForks_2009
union
select '2010' as year
,count(distinct mf_id) projectCount
from fsoc.pfwithForks_2010
union
select '2011' as year
,count(distinct mf_id) projectCount
from fsoc.pfwithForks_2011
union
select '2012' as year
,count(distinct mf_id) projectCount
from fsoc.pfwithForks_2012;

#  Projects with independently developed forks (MF-PF pairs)
select '2008' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.pfwithForks_2008
union
select '2009' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.pfwithForks_2009
union
select '2010' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.pfwithForks_2010
union
select '2011' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.pfwithForks_2011
union
select '2012' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.pfwithForks_2012;

# Projects developed within
select '2008' as year
,count(distinct mf_id) projectCount
from fsoc.commithistorystatistics_within_2008
union
select '2009' as year
,count(distinct mf_id) projectCount
from fsoc.commithistorystatistics_within_2009
union
select '2010' as year
,count(distinct mf_id) projectCount
from fsoc.commithistorystatistics_within_2010
union
select '2011' as year
,count(distinct mf_id) projectCount
from fsoc.commithistorystatistics_within_2011
union
select '2012' as year
,count(distinct mf_id) projectCount
from fsoc.commithistorystatistics_within_2012;

# Projects developed Outside
select '2008' as year
,count(distinct mf_id) projectCount
from fsoc.commithistorystatistics_outside_2008
union
select '2009' as year
,count(distinct mf_id) projectCount
from fsoc.commithistorystatistics_outside_2009
union
select '2010' as year
,count(distinct mf_id) projectCount
from fsoc.commithistorystatistics_outside_2010
union
select '2011' as year
,count(distinct mf_id) projectCount
from fsoc.commithistorystatistics_outside_2011
union
select '2012' as year
,count(distinct mf_id) projectCount
from fsoc.commithistorystatistics_outside_2012;

####################################################

# Projects developed within
select '2008' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.commithistorystatistics_within_2008
union
select '2009' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.commithistorystatistics_within_2009
union
select '2010' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.commithistorystatistics_within_2010
union
select '2011' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.commithistorystatistics_within_2011
union
select '2012' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.commithistorystatistics_within_2012;

# Projects developed Outside
select '2008' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.commithistorystatistics_outside_2008
union
select '2009' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.commithistorystatistics_outside_2009
union
select '2010' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.commithistorystatistics_outside_2010
union
select '2011' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.commithistorystatistics_outside_2011
union
select '2012' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.commithistorystatistics_outside_2012;


# Projects with significant forks within
select '2008' as year
,count(distinct mf_id) projectCount
from fsoc.idswithsignificantforks_within_2008
union
select '2009' as year
,count(distinct mf_id) projectCount
from fsoc.idswithsignificantforks_within_2009
union
select '2010' as year
,count(distinct mf_id) projectCount
from fsoc.idswithsignificantforks_within_2010
union
select '2011' as year
,count(distinct mf_id) projectCount
from fsoc.idswithsignificantforks_within_2011
union
select '2012' as year
,count(distinct mf_id) projectCount
from fsoc.idswithsignificantforks_within_2012;

# Projects with significant forks Outside
select '2008' as year
,count(distinct mf_id) projectCount
from fsoc.idswithsignificantforks_outside_2008
union
select '2009' as year
,count(distinct mf_id) projectCount
from fsoc.idswithsignificantforks_outside_2009
union
select '2010' as year
,count(distinct mf_id) projectCount
from fsoc.idswithsignificantforks_outside_2010
union
select '2011' as year
,count(distinct mf_id) projectCount
from fsoc.idswithsignificantforks_outside_2011
union
select '2012' as year
,count(distinct mf_id) projectCount
from fsoc.idswithsignificantforks_outside_2012;

####################################################

# Projects with significant forks within
select '2008' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.idswithsignificantforks_within_2008
union
select '2009' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.idswithsignificantforks_within_2009
union
select '2010' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.idswithsignificantforks_within_2010
union
select '2011' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.idswithsignificantforks_within_2011
union
select '2012' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.idswithsignificantforks_within_2012;

# Projects with significant forks Outside
select '2008' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.idswithsignificantforks_outside_2008
union
select '2009' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.idswithsignificantforks_outside_2009
union
select '2010' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.idswithsignificantforks_outside_2010
union
select '2011' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.idswithsignificantforks_outside_2011
union
select '2012' as year
,count(distinct mf_id,pf_id) projectCount
from fsoc.idswithsignificantforks_outside_2012;

################################

select '2008' as year
,count(distinct mf_id) projectCount
from (select *
from fsoc.idswithsignificantforks_within_2008
union 
select *
from fsoc.idswithsignificantforks_outside_2008
) a
union
select '2009' as year
,count(distinct mf_id) projectCount
from (select *
from fsoc.idswithsignificantforks_within_2009
union 
select *
from fsoc.idswithsignificantforks_outside_2009
) a
union
select '2010' as year
,count(distinct mf_id) projectCount
from (select *
from fsoc.idswithsignificantforks_within_2010
union 
select *
from fsoc.idswithsignificantforks_outside_2010
) a
union
select '2011' as year
,count(distinct mf_id) projectCount
from (select *
from fsoc.idswithsignificantforks_within_2011
union 
select *
from fsoc.idswithsignificantforks_outside_2011
) a
union
select '2012' as year
,count(distinct mf_id) projectCount
from (select *
from fsoc.idswithsignificantforks_within_2012
union 
select *
from fsoc.idswithsignificantforks_outside_2012
) a;






