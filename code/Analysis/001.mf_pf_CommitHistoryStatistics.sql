# 001
delimiter $$
drop procedure if exists fsoc.mf_pf_CommitHistoryStatistics$$
create procedure fsoc.mf_pf_CommitHistoryStatistics(in yr int)
begin
set @st=concat("drop table if exists fsoc.mf_CommitHistoryStatistics_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.mf_CommitHistoryStatistics_",yr," ",
"select mf_id ",
",count(commit_id) commit_count ",
",count(distinct author_id) author_count ",
",count(distinct committer_id) committer_count ",
",min(created_at) min_commit_at ",
",max(created_at) max_commit_at ",
",timestampdiff(day,min(created_at),max(created_at)) DurationInDays ",
"from fsoc.commithistoryofmf_",yr," ",
"group by MF_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("drop table if exists fsoc.pf_CommitHistoryStatistics_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.pf_CommitHistoryStatistics_",yr," ",
"select MF_id ",
",PF_id ",
",count(commit_id) commit_count ",
",count(distinct author_id) author_count ",
",count(distinct committer_id) committer_count ",
",min(created_at) min_commit_at ",
",max(created_at) max_commit_at ",
",timestampdiff(day,min(created_at),max(created_at)) DurationInDays ",
"from fsoc.commithistoryofpf_",yr," ",
"group by MF_id,PF_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.mf_pf_CommitHistoryStatistics_indexes$$
create procedure fsoc.mf_pf_CommitHistoryStatistics_indexes(in yr int)
begin
set @st=concat("alter table fsoc.mf_CommitHistoryStatistics_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.pf_CommitHistoryStatistics_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.pf_CommitHistoryStatistics_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;

