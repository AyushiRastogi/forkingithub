drop table fsoc.forkingStatistics_2008;
create table fsoc.forkingStatistics_2008
select distinct sf.MF_id
,ifnull(mf_forkCount,0) mf_forkCount
,mf_domain
,ifnull(mf_watchersCount,0) mf_watchersCount
,MF_owner
,ifnull(mf_followersCount,0) mf_followersCount
,mf_language
,ifnull(mf_stats.author_count,0) mf_authorCount
,ifnull(mf_stats.committer_count,0) mf_committerCount
,ifnull(mf_stats.commit_count,0) mf_commitCount
,MF_createdAt
,mf_stats.min_commit_at mf_minCommitAt
,mf_stats.max_commit_at mf_maxCommitAt
,sf.PF_id
,pf_createdAt
,timestampdiff(day,if(mf_stats.min_commit_at is not null and mf_createdAt>mf_stats.min_commit_at,if(mf_stats.min_commit_at='0000:00:00',mf_createdAt,mf_stats.min_commit_at),mf_createdAt),
if(pf_stats.min_commit_at is not null and pf_createdAt>pf_stats.min_commit_at,if(pf_stats.min_commit_at='0000:00:00',pf_createdAt,pf_stats.min_commit_at),pf_createdAt)) MF_Age
,ifnull(pf_watchersCount,0) pf_watchersCount
,ifnull(pf_owner,0) pf_owner
,ifnull(pf_followersCount,0) pf_followersCount
,sf.pF_language
,ifnull(pf_stats.author_count,0) pf_authorCount
,ifnull(pf_stats.committer_count,0) pf_committerCount
,ifnull(pf_stats.commit_count,0) pf_commitCount
,pf_stats.min_commit_at pf_minCommitAt
,pf_stats.max_commit_at pf_maxCommitAt
,if(pf_stats.commit_count>=100 and pf_stats.DurationInDays>=90 and pf_stats.committer_count>=2 and pf_stats.author_count>=4,1,0) SignificantFork
,if(wn.pf_id is null and ot.pf_id is null,0,1) Less
from fsoc.presf_2008 sf
left join fsoc.mf_CommitHistoryStatistics_2008 mf_stats
on mf_stats.mf_id=sf.MF_id
left join fsoc.pf_CommitHistoryStatistics_2008 pf_stats
on pf_stats.mf_id=sf.MF_id
and pf_stats.pf_id=sf.PF_id
left join fsoc.pf_id_less_within wn
on wn.pf_id=sf.PF_id
left join fsoc.pf_id_less_outside ot
on ot.pf_id=sf.PF_id;

###########################
drop table fsoc.forkingStatistics_2009;
create table fsoc.forkingStatistics_2009
select distinct sf.MF_id
,ifnull(mf_forkCount,0) mf_forkCount
,mf_domain
,ifnull(mf_watchersCount,0) mf_watchersCount
,MF_owner
,ifnull(mf_followersCount,0) mf_followersCount
,mf_language
,ifnull(mf_stats.author_count,0) mf_authorCount
,ifnull(mf_stats.committer_count,0) mf_committerCount
,ifnull(mf_stats.commit_count,0) mf_commitCount
,MF_createdAt
,mf_stats.min_commit_at mf_minCommitAt
,mf_stats.max_commit_at mf_maxCommitAt
,sf.PF_id
,pf_createdAt
,timestampdiff(day,if(mf_stats.min_commit_at is not null and mf_createdAt>mf_stats.min_commit_at,if(mf_stats.min_commit_at='0000:00:00',mf_createdAt,mf_stats.min_commit_at),mf_createdAt),
if(pf_stats.min_commit_at is not null and pf_createdAt>pf_stats.min_commit_at,if(pf_stats.min_commit_at='0000:00:00',pf_createdAt,pf_stats.min_commit_at),pf_createdAt)) MF_Age
,ifnull(pf_watchersCount,0) pf_watchersCount
,ifnull(pf_owner,0) pf_owner
,ifnull(pf_followersCount,0) pf_followersCount
,sf.pF_language
,ifnull(pf_stats.author_count,0) pf_authorCount
,ifnull(pf_stats.committer_count,0) pf_committerCount
,ifnull(pf_stats.commit_count,0) pf_commitCount
,pf_stats.min_commit_at pf_minCommitAt
,pf_stats.max_commit_at pf_maxCommitAt
,if(pf_stats.commit_count>=100 and pf_stats.DurationInDays>=90 and pf_stats.committer_count>=2 and pf_stats.author_count>=4,1,0) SignificantFork
,if(wn.pf_id is null and ot.pf_id is null,0,1) Less
from fsoc.presf_2009 sf
left join fsoc.mf_CommitHistoryStatistics_2009 mf_stats
on mf_stats.mf_id=sf.MF_id
left join fsoc.pf_CommitHistoryStatistics_2009 pf_stats
on pf_stats.mf_id=sf.MF_id
and pf_stats.pf_id=sf.PF_id
left join fsoc.pf_id_less_within wn
on wn.pf_id=sf.PF_id
left join fsoc.pf_id_less_outside ot
on ot.pf_id=sf.PF_id;


###########################

drop table fsoc.forkingStatistics_2010;
create table fsoc.forkingStatistics_2010
select distinct sf.MF_id
,ifnull(mf_forkCount,0) mf_forkCount
,mf_domain
,ifnull(mf_watchersCount,0) mf_watchersCount
,MF_owner
,ifnull(mf_followersCount,0) mf_followersCount
,mf_language
,ifnull(mf_stats.author_count,0) mf_authorCount
,ifnull(mf_stats.committer_count,0) mf_committerCount
,ifnull(mf_stats.commit_count,0) mf_commitCount
,MF_createdAt
,mf_stats.min_commit_at mf_minCommitAt
,mf_stats.max_commit_at mf_maxCommitAt
,sf.PF_id
,pf_createdAt
,timestampdiff(day,if(mf_stats.min_commit_at is not null and mf_createdAt>mf_stats.min_commit_at,if(mf_stats.min_commit_at='0000:00:00',mf_createdAt,mf_stats.min_commit_at),mf_createdAt),
if(pf_stats.min_commit_at is not null and pf_createdAt>pf_stats.min_commit_at,if(pf_stats.min_commit_at='0000:00:00',pf_createdAt,pf_stats.min_commit_at),pf_createdAt)) MF_Age
,ifnull(pf_watchersCount,0) pf_watchersCount
,ifnull(pf_owner,0) pf_owner
,ifnull(pf_followersCount,0) pf_followersCount
,sf.pF_language
,ifnull(pf_stats.author_count,0) pf_authorCount
,ifnull(pf_stats.committer_count,0) pf_committerCount
,ifnull(pf_stats.commit_count,0) pf_commitCount
,pf_stats.min_commit_at pf_minCommitAt
,pf_stats.max_commit_at pf_maxCommitAt
,if(pf_stats.commit_count>=100 and pf_stats.DurationInDays>=90 and pf_stats.committer_count>=2 and pf_stats.author_count>=4,1,0) SignificantFork
,if(wn.pf_id is null and ot.pf_id is null,0,1) Less
from fsoc.presf_2010 sf
left join fsoc.mf_CommitHistoryStatistics_2010 mf_stats
on mf_stats.mf_id=sf.MF_id
left join fsoc.pf_CommitHistoryStatistics_2010 pf_stats
on pf_stats.mf_id=sf.MF_id
and pf_stats.pf_id=sf.PF_id
left join fsoc.pf_id_less_within wn
on wn.pf_id=sf.PF_id
left join fsoc.pf_id_less_outside ot
on ot.pf_id=sf.PF_id;


######################################
drop table fsoc.forkingStatistics_2011;
create table fsoc.forkingStatistics_2011
select distinct sf.MF_id
,ifnull(mf_forkCount,0) mf_forkCount
,mf_domain
,ifnull(mf_watchersCount,0) mf_watchersCount
,MF_owner
,ifnull(mf_followersCount,0) mf_followersCount
,mf_language
,ifnull(mf_stats.author_count,0) mf_authorCount
,ifnull(mf_stats.committer_count,0) mf_committerCount
,ifnull(mf_stats.commit_count,0) mf_commitCount
,MF_createdAt
,mf_stats.min_commit_at mf_minCommitAt
,mf_stats.max_commit_at mf_maxCommitAt
,sf.PF_id
,pf_createdAt
,timestampdiff(day,if(mf_stats.min_commit_at is not null and mf_createdAt>mf_stats.min_commit_at,if(mf_stats.min_commit_at='0000:00:00',mf_createdAt,mf_stats.min_commit_at),mf_createdAt),
if(pf_stats.min_commit_at is not null and pf_createdAt>pf_stats.min_commit_at,if(pf_stats.min_commit_at='0000:00:00',pf_createdAt,pf_stats.min_commit_at),pf_createdAt)) MF_Age
,ifnull(pf_watchersCount,0) pf_watchersCount
,ifnull(pf_owner,0) pf_owner
,ifnull(pf_followersCount,0) pf_followersCount
,sf.pF_language
,ifnull(pf_stats.author_count,0) pf_authorCount
,ifnull(pf_stats.committer_count,0) pf_committerCount
,ifnull(pf_stats.commit_count,0) pf_commitCount
,pf_stats.min_commit_at pf_minCommitAt
,pf_stats.max_commit_at pf_maxCommitAt
,if(pf_stats.commit_count>=100 and pf_stats.DurationInDays>=90 and pf_stats.committer_count>=2 and pf_stats.author_count>=4,1,0) SignificantFork
,if(wn.pf_id is null and ot.pf_id is null,0,1) Less
from fsoc.presf_2011 sf
left join fsoc.mf_CommitHistoryStatistics_2011 mf_stats
on mf_stats.mf_id=sf.MF_id
left join fsoc.pf_CommitHistoryStatistics_2011 pf_stats
on pf_stats.mf_id=sf.MF_id
and pf_stats.pf_id=sf.PF_id
left join fsoc.pf_id_less_within wn
on wn.pf_id=sf.PF_id
left join fsoc.pf_id_less_outside ot
on ot.pf_id=sf.PF_id;

##############################
drop table fsoc.forkingStatistics_2012;
create table fsoc.forkingStatistics_2012
select distinct sf.MF_id
,ifnull(mf_forkCount,0) mf_forkCount
,mf_domain
,ifnull(mf_watchersCount,0) mf_watchersCount
,MF_owner
,ifnull(mf_followersCount,0) mf_followersCount
,mf_language
,ifnull(mf_stats.author_count,0) mf_authorCount
,ifnull(mf_stats.committer_count,0) mf_committerCount
,ifnull(mf_stats.commit_count,0) mf_commitCount
,MF_createdAt
,mf_stats.min_commit_at mf_minCommitAt
,mf_stats.max_commit_at mf_maxCommitAt
,sf.PF_id
,pf_createdAt
,timestampdiff(day,if(mf_stats.min_commit_at is not null and mf_createdAt>mf_stats.min_commit_at,if(mf_stats.min_commit_at='0000:00:00',mf_createdAt,mf_stats.min_commit_at),mf_createdAt),
if(pf_stats.min_commit_at is not null and pf_createdAt>pf_stats.min_commit_at,if(pf_stats.min_commit_at='0000:00:00',pf_createdAt,pf_stats.min_commit_at),pf_createdAt)) MF_Age
,ifnull(pf_watchersCount,0) pf_watchersCount
,ifnull(pf_owner,0) pf_owner
,ifnull(pf_followersCount,0) pf_followersCount
,sf.pF_language
,ifnull(pf_stats.author_count,0) pf_authorCount
,ifnull(pf_stats.committer_count,0) pf_committerCount
,ifnull(pf_stats.commit_count,0) pf_commitCount
,pf_stats.min_commit_at pf_minCommitAt
,pf_stats.max_commit_at pf_maxCommitAt
,if(pf_stats.commit_count>=100 and pf_stats.DurationInDays>=90 and pf_stats.committer_count>=2 and pf_stats.author_count>=4,1,0) SignificantFork
,if(wn.pf_id is null and ot.pf_id is null,0,1) Less
from fsoc.presf_2012 sf
left join fsoc.mf_CommitHistoryStatistics_2012 mf_stats
on mf_stats.mf_id=sf.MF_id
left join fsoc.pf_CommitHistoryStatistics_2012 pf_stats
on pf_stats.mf_id=sf.MF_id
and pf_stats.pf_id=sf.PF_id
left join fsoc.pf_id_less_within wn
on wn.pf_id=sf.PF_id
left join fsoc.pf_id_less_outside ot
on ot.pf_id=sf.PF_id;

















