create table fsoc.mf_pf_id_less_within
select forked_from mf_id,pf_id
from fsoc.pf_id_less_within
left join github.projects 
on pf_id=id;

create table fsoc.mf_pf_id_greater_within
select forked_from mf_id,pf_id
from fsoc.pf_id_greater_within
left join github.projects 
on pf_id=id;

create table fsoc.mf_pf_id_equal_within
select forked_from mf_id,pf_id
from fsoc.pf_id_equal_within
left join github.projects 
on pf_id=id;

create table fsoc.mf_pf_id_less_outside
select forked_from mf_id,pf_id
from fsoc.pf_id_less_outside
left join github.projects 
on pf_id=id;

create table fsoc.mf_pf_id_greater_outside
select forked_from mf_id,pf_id
from fsoc.pf_id_greater_outside
left join github.projects 
on pf_id=id;

create table fsoc.mf_pf_id_equal_outside
select forked_from mf_id,pf_id
from fsoc.pf_id_equal_outside
left join github.projects 
on pf_id=id;

###############################
select distinct wthn.mf_id
from fsoc.mf_pf_id_less_within wthn
,fsoc.mf_pf_id_less_outside outsd
where wthn.mf_id=outsd.mf_id;

select distinct ls.mf_id
from fsoc.mf_pf_id_less_within ls
,fsoc.mf_pf_id_greater_within gr
where ls.mf_id=gr.mf_id;

select distinct ls.mf_id
from fsoc.mf_pf_id_less_within ls
,fsoc.mf_pf_id_greater_outside gr
where ls.mf_id=gr.mf_id;

select distinct ls.mf_id
from fsoc.mf_pf_id_less_outside ls
,fsoc.mf_pf_id_greater_outside gr
where ls.mf_id=gr.mf_id;

select distinct ls.mf_id
from fsoc.mf_pf_id_less_outside ls
,fsoc.mf_pf_id_greater_within gr
where ls.mf_id=gr.mf_id;

select distinct ls.mf_id
from fsoc.mf_pf_id_less_within ls
,fsoc.mf_pf_id_equal_within eq
where ls.mf_id=eq.mf_id;



