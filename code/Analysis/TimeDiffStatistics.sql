select mf_id
,pf_id
,'before' as type
,count(*) time_interval
,round(avg(time_diff),1) td
,min(time_diff) mtd
,max(time_diff) matd
from fsoc.before_SerializedTimeDiffCommitPatternQ1_Within_2008
where pf_id in(
select bfr.pf_id
from fsoc.before_SerializedTimeDiffCommitPatternQ1_Within_2008 bfr
,fsoc.after_SerializedTimeDiffCommitPatternQ1_Within_2008 aftr
where bfr.mf_id=aftr.mf_id
and bfr.pf_id=aftr.pf_id
group by pf_id
having max(bfr.SNo)-min(bfr.Sno)+1>=3 and max(aftr.SNo)-min(aftr.Sno)+1>=3)
group by mf_id,pf_id
union
select mf_id
,pf_id
,'after' as type
,count(*) time_interval
,round(avg(time_diff),1) td
,min(time_diff) mtd
,max(time_diff) matd
from fsoc.after_SerializedTimeDiffCommitPatternQ1_Within_2008
where pf_id in(
select bfr.pf_id
from fsoc.before_SerializedTimeDiffCommitPatternQ1_Within_2008 bfr
,fsoc.after_SerializedTimeDiffCommitPatternQ1_Within_2008 aftr
where bfr.mf_id=aftr.mf_id
and bfr.pf_id=aftr.pf_id
group by pf_id
having max(bfr.SNo)-min(bfr.Sno)+1>=3 and max(aftr.SNo)-min(aftr.Sno)+1>=3)
group by mf_id,pf_id
order by mf_id,pf_id,type;
