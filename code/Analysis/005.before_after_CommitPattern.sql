delimiter $$
drop procedure if exists fsoc.before_after_CommitPattern$$
create procedure fsoc.before_after_CommitPattern(in yr int,quarter int)
begin
set @st=concat("drop table if exists fsoc.before_CommitPatternQ",quarter,"_Within_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.before_CommitPatternQ",quarter,"_Within_",yr," ",
"select ids.mf_id ",
",pf_id ",
",year(created_at) year ",
",lpad(month(created_at),2,0) month ",
",count(commit_id) commit_count ",
"from fsoc.IdsWithSigDevHistQ",quarter,"_Within_",yr," ids ",
"left join fsoc.commithistoryofmf_",yr," his ",
"on his.mf_id=ids.mf_id ",
"where date_format(created_at,'%Y-%m')<=date_format(break,'%Y-%m') ",
"group by ids.mf_id,pf_id,year,month ",
"order by ids.mf_id,pf_id,year,month");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("drop table if exists fsoc.after_CommitPatternQ",quarter,"_Within_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.after_CommitPatternQ",quarter,"_Within_",yr," ",
"select ids.mf_id ",
",pf_id ",
",year(created_at) year ",
",lpad(month(created_at),2,0) month ",
",count(commit_id) commit_count ",
"from fsoc.IdsWithSigDevHistQ",quarter,"_Within_",yr," ids ",
"left join fsoc.commithistoryofmf_",yr," his ",
"on his.mf_id=ids.mf_id ",
"where date_format(created_at,'%Y-%m')>date_format(break,'%Y-%m') ",
"group by ids.mf_id,pf_id,year,month ",
"order by ids.mf_id,pf_id,year,month");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("drop table if exists fsoc.before_CommitPatternQ",quarter,"_Outside_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.before_CommitPatternQ",quarter,"_Outside_",yr," ",
"select ids.mf_id ",
",pf_id ",
",year(created_at) year ",
",lpad(month(created_at),2,0) month ",
",count(commit_id) commit_count ",
"from fsoc.IdsWithSigDevHistQ",quarter,"_Outside_",yr," ids ",
"left join fsoc.commithistoryofmf_",yr," his ",
"on his.mf_id=ids.mf_id ",
"where date_format(created_at,'%Y-%m')<=date_format(break,'%Y-%m') ",
"group by ids.mf_id,pf_id,year,month ",
"order by ids.mf_id,pf_id,year,month");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("drop table if exists fsoc.after_CommitPatternQ",quarter,"_Outside_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.after_CommitPatternQ",quarter,"_Outside_",yr," ",
"select ids.mf_id ",
",pf_id ",
",year(created_at) year ",
",lpad(month(created_at),2,0) month ",
",count(commit_id) commit_count ",
"from fsoc.IdsWithSigDevHistQ",quarter,"_Outside_",yr," ids ",
"left join fsoc.commithistoryofmf_",yr," his ",
"on his.mf_id=ids.mf_id ",
"where date_format(created_at,'%Y-%m')>date_format(break,'%Y-%m') ",
"group by ids.mf_id,pf_id,year,month ",
"order by ids.mf_id,pf_id,year,month");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.before_after_CommitPattern_indexes$$
create procedure fsoc.before_after_CommitPattern_indexes(in yr int,quarter int)
begin
set @st=concat("alter table fsoc.before_CommitPatternQ",quarter,"_Within_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.before_CommitPatternQ",quarter,"_Within_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.after_CommitPatternQ",quarter,"_Within_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.after_CommitPatternQ",quarter,"_Within_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.before_CommitPatternQ",quarter,"_Outside_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.before_CommitPatternQ",quarter,"_Outside_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.after_CommitPatternQ",quarter,"_Outside_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.after_CommitPatternQ",quarter,"_Outside_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;

