delimiter $$
drop procedure if exists fsoc.before_after_SerializedTimeDiffCommitPattern$$
create procedure fsoc.before_after_SerializedTimeDiffCommitPattern(in yr int,quarter int)
begin

set @st=concat("drop table if exists fsoc.before_SerializedTimeDiffCommitPatternQ",quarter,"_Within_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.before_SerializedTimeDiffCommitPatternQ",quarter,"_Within_",yr," ",
"select a.SNo ",
",a.mf_id ",
",a.pf_id ",
",(12*(b.year-a.year)+(b.month-a.month)) time_diff ",
"from fsoc.before_Serializedcommitpatternq",quarter,"_within_",yr," a ",
"left join fsoc.before_Serializedcommitpatternq",quarter,"_within_",yr," b ",
"on a.mf_id=b.mf_id ",
"and a.pf_id=b.pf_id ",
"where b.SNo=a.SNo+1");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;


set @st=concat("drop table if exists fsoc.after_SerializedTimeDiffCommitPatternQ",quarter,"_Within_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.after_SerializedTimeDiffCommitPatternQ",quarter,"_Within_",yr," ",
"select a.SNo ",
",a.mf_id ",
",a.pf_id ",
",(12*(b.year-a.year)+(b.month-a.month)) time_diff ",
"from fsoc.after_Serializedcommitpatternq",quarter,"_within_",yr," a ",
"left join fsoc.after_Serializedcommitpatternq",quarter,"_within_",yr," b ",
"on a.mf_id=b.mf_id ",
"and a.pf_id=b.pf_id ",
"where b.SNo=a.SNo+1");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;


set @st=concat("drop table if exists fsoc.before_SerializedTimeDiffCommitPatternQ",quarter,"_Outside_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.before_SerializedTimeDiffCommitPatternQ",quarter,"_Outside_",yr," ",
"select a.SNo ",
",a.mf_id ",
",a.pf_id ",
",(12*(b.year-a.year)+(b.month-a.month)) time_diff ",
"from fsoc.before_Serializedcommitpatternq",quarter,"_outside_",yr," a ",
"left join fsoc.before_Serializedcommitpatternq",quarter,"_outside_",yr," b ",
"on a.mf_id=b.mf_id ",
"and a.pf_id=b.pf_id ",
"where b.SNo=a.SNo+1");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;


set @st=concat("drop table if exists fsoc.after_SerializedTimeDiffCommitPatternQ",quarter,"_Outside_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.after_SerializedTimeDiffCommitPatternQ",quarter,"_Outside_",yr," ",
"select a.SNo ",
",a.mf_id ",
",a.pf_id ",
",(12*(b.year-a.year)+(b.month-a.month)) time_diff ",
"from fsoc.after_Serializedcommitpatternq",quarter,"_outside_",yr," a ",
"left join fsoc.after_Serializedcommitpatternq",quarter,"_outside_",yr," b ",
"on a.mf_id=b.mf_id ",
"and a.pf_id=b.pf_id ",
"where b.SNo=a.SNo+1");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.before_after_SerializedTimeDiffCommitPattern_indexes$$
create procedure fsoc.before_after_SerializedTimeDiffCommitPattern_indexes(in yr int,quarter int)
begin
set @st=concat("alter table fsoc.before_SerializedTimeDiffCommitPatternQ",quarter,"_Within_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.before_SerializedTimeDiffCommitPatternQ",quarter,"_Within_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.after_SerializedTimeDiffCommitPatternQ",quarter,"_Within_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.after_SerializedTimeDiffCommitPatternQ",quarter,"_Within_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.before_SerializedTimeDiffCommitPatternQ",quarter,"_Outside_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.before_SerializedTimeDiffCommitPatternQ",quarter,"_Outside_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.after_SerializedTimeDiffCommitPatternQ",quarter,"_Outside_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.after_SerializedTimeDiffCommitPatternQ",quarter,"_Outside_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;

