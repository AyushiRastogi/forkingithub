delimiter $$
drop procedure if exists fsoc.preForkSFWithNoPR$$
create procedure fsoc.preForkSFWithNoPR(in yr int)
begin
set @st=concat("drop table if exists fsoc.preForkSFWithNoPR_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.preForkSFWithNoPR_",yr," ",
"select distinct SF.MF_id ",
",SF.PF_id ",
",SF.SF_id ",
",NULL as user_id ",
",'SF With No PR' as type ",
",NULL as pull_request_id ",
",NULL as merged ",
"from fsoc.preforkType_",yr," frk ",
"right join fsoc.preSF_",yr," SF ",
"on SF.MF_id=frk.MF_id ",
"and SF.PF_id=frk.PF_id ",
"and SF.SF_id!=frk.SF_id ",
"where frk.PF_id is not null");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.preForkSFWithNoPR_indexes$$
create procedure fsoc.preForkSFWithNoPR_indexes(in yr int)
begin
set @st=concat("alter table fsoc.preForkSFWithNoPR_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
set @st=concat("alter table fsoc.preForkSFWithNoPR_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.preForkSFWithNoPR_",yr, " ",
"add index SF_id_",yr,"(SF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;
