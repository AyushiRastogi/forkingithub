delimiter $$
drop procedure if exists fsoc.pre_MF$$
create procedure fsoc.pre_MF(in yr int)
begin
set @st=concat("drop table if exists fsoc.pre_MF_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.pre_MF_",yr," ",
"select id as MF_id ",
",created_at as MF_created_at ",
"from github.projects ",
"where forked_from is null ", -- base repository
"and year(created_at)='",yr,"' ",
"and deleted=0");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.pre_MF_indexes$$
create procedure fsoc.pre_MF_indexes(in yr int)
begin
set @st=concat("alter table fsoc.pre_MF_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
end$$
delimiter ;











