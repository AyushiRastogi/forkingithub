delimiter $$
drop procedure if exists fsoc.CommitHistory$$
create procedure fsoc.CommitHistory(in yr int)
begin
set @st=concat("drop table if exists fsoc.CommitHistoryOfMF_IndDevPF_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.CommitHistoryOfMF_IndDevPF_",yr," ",
"select mf_id ",
",id commit_id ",
",author_id ",
",committer_id ",
",created_at ",
"from fsoc.mfwithindependentdevelopment_",yr," id ",
"left join github.commits comm ",
"on comm.project_id=id.mf_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("drop table if exists fsoc.CommitHistoryOfMF_ExoIntra_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.CommitHistoryOfMF_ExoIntra_",yr," ",
"select mf_id ",
",id commit_id ",
",author_id ",
",committer_id ",
",created_at ",
"from fsoc.mfwithexointraforks_",yr," frk ",
"left join github.commits comm ",
"on comm.project_id=frk.mf_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("drop table if exists fsoc.CommitHistoryOfPF_ExoIntra_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.CommitHistoryOfPF_ExoIntra_",yr," ",
"select mf_id ",
",pf_id ",
",id commit_id ",
",author_id ",
",committer_id ",
",created_at ",
"from fsoc.pfwithexointraforks_",yr," frk ",
"left join github.commits comm ",
"on comm.project_id=frk.pf_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("drop table if exists fsoc.CommitHistoryOfMF_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.CommitHistoryOfMF_",yr," ",
"select * ",
"from fsoc.commithistoryofmf_exointra_",yr," ",
"union ",
"select * ",
"from fsoc.commithistoryofmf_inddevpf_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("drop table if exists fsoc.CommitHistoryOfPF_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.CommitHistoryOfPF_",yr," ",
"select * ",
"from fsoc.commithistoryofpf_exointra_",yr," ",
"union ",
"select * ",
"from fsoc.commithistoryofpf_inddevpf_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.CommitHistory_indexes$$
create procedure fsoc.CommitHistory_indexes(in yr int)
begin
set @st=concat("alter table fsoc.CommitHistoryOfMF_IndDevPF_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

/*
set @st=concat("alter table fsoc.CommitHistoryOfPF_IndDevPF_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.CommitHistoryOfPF_IndDevPF_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;*/

##############################

set @st=concat("alter table fsoc.CommitHistoryOfMF_ExoIntra_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.CommitHistoryOfPF_ExoIntra_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.CommitHistoryOfPF_ExoIntra_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;

############################
set @st=concat("alter table fsoc.CommitHistoryOfMF_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.CommitHistoryOfPF_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.CommitHistoryOfPF_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;