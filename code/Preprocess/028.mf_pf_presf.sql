delimiter $$
drop procedure if exists fsoc.mf_pf_presf$$
create procedure fsoc.mf_pf_presf(in yr int)
begin
set @st=concat("drop table if exists fsoc.presf_mf_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.presf_mf_",yr," ",
"select distinct mf_id ",
"from fsoc.presf_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("drop table if exists fsoc.presf_pf_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.presf_pf_",yr," ",
"select distinct mf_id,pf_id ",
"from fsoc.presf_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.mf_pf_presf_indexes$$
create procedure fsoc.mf_pf_presf_indexes(in yr int)
begin
set @st=concat("alter table fsoc.presf_mf_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.presf_pf_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.presf_pf_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;

