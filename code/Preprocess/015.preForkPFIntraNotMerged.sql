delimiter $$
drop procedure if exists fsoc.preForkPFIntraNotMerged$$
create procedure fsoc.preForkPFIntraNotMerged(in yr int)
begin
set @st=concat("drop table if exists fsoc.preForkPFIntraNotMerged_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.preForkPFIntraNotMerged_",yr," ",
"select pfi.* ",
"from fsoc.preForkPFIntra_",yr," pfi ",
"left join fsoc.preForkMerged_",yr," mrg ",
"on mrg.MF_id=pfi.MF_id ",
"where mrg.PF_id!=pfi.PF_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.preForkPFIntraNotMerged_indexes$$
create procedure fsoc.preForkPFIntraNotMerged_indexes(in yr int)
begin
set @st=concat("alter table fsoc.preForkPFIntraNotMerged_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
set @st=concat("alter table fsoc.preForkPFIntraNotMerged_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;
