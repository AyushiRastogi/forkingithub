delimiter $$
drop procedure if exists fsoc.mf_pf_presfLanguage$$
create procedure fsoc.mf_pf_presfLanguage(in yr int)
begin
/*
set @st=concat("alter table fsoc.presf_",yr," ",
"drop column mf_language");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.presf_",yr," ",
"drop column pf_language");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.presf_",yr," ",
"drop column sf_language");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;*/

set @st=concat("alter table fsoc.presf_",yr," ",
"add column mf_language varchar(20) null after mf_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.presf_",yr," ",
"add column pf_language varchar(20) null after pf_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.presf_",yr," ",
"add column sf_language varchar(20) null after sf_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

SET SQL_SAFE_UPDATES = 0;

set @st=concat("update fsoc.presf_",yr," sf ",
"left join github.projects proj ",
"on sf.mf_id=proj.id ",
"set mf_language=language");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("update fsoc.presf_",yr," sf ",
"left join github.projects proj ",
"on sf.pf_id=proj.id ",
"set pf_language=language");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("update fsoc.presf_",yr," sf ",
"left join github.projects proj ",
"on sf.sf_id=proj.id ",
"set sf_language=language");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;


