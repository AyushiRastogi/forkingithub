delimiter $$
drop procedure if exists fsoc.mf_pf_presfFollowersCount$$
create procedure fsoc.mf_pf_presfFollowersCount(in yr int)
begin
/*
set @st=concat("alter table fsoc.presf_",yr," ",
"drop column mf_FollowersCount");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.presf_",yr," ",
"drop column pf_FollowersCount");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.presf_",yr," ",
"drop column sf_FollowersCount");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
*/
set @st=concat("alter table fsoc.presf_",yr," ",
"add column mf_FollowersCount varchar(20) null after mf_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.presf_",yr," ",
"add column pf_FollowersCount varchar(20) null after pf_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.presf_",yr," ",
"add column sf_FollowersCount varchar(20) null after sf_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

SET SQL_SAFE_UPDATES = 0;

set @st=concat("update fsoc.presf_",yr," sf ",
"left join fsoc.Followers_count fc ",
"on fc.user_id=sf.MF_Owner ",
"set mf_FollowersCount=Followers_Count");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("update fsoc.presf_",yr," sf ",
"left join fsoc.Followers_count fc ",
"on fc.user_id=sf.PF_Owner ",
"set pf_FollowersCount=Followers_Count");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("update fsoc.presf_",yr," sf ",
"left join fsoc.Followers_count fc ",
"on fc.user_id=sf.SF_Owner ",
"set sf_FollowersCount=Followers_Count");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;


