delimiter $$
drop procedure if exists fsoc.MF_PF_WithForks$$
create procedure fsoc.MF_PF_WithForks(in yr int)
begin
set @st=concat("drop table if exists fsoc.MFWithForks_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.MFWithForks_",yr," ",
"select MF_id ",
"from fsoc.mfwithExoIntraforks_",yr," ",
"union ",
"select MF_id ",
"from fsoc.MFWithIndependentDevelopment_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("drop table if exists fsoc.PFWithForks_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.PFWithForks_",yr," ",
"select MF_id,PF_id ",
"from fsoc.pfwithExoIntraforks_",yr," ",
"union ",
"select MF_id,PF_id ",
"from fsoc.PFWithIndependentDevelopment_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.MF_PF_WithForks_indexes$$
create procedure fsoc.MF_PF_WithForks_indexes(in yr int)
begin
set @st=concat("alter table fsoc.MFWithForks_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.PFWithForks_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.PFWithForks_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
end$$
delimiter ;

