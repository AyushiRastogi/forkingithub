delimiter $$
drop procedure if exists fsoc.mf_pf_ProjectMembers$$
create procedure fsoc.mf_pf_ProjectMembers(in yr int)
begin
set @st=concat("drop table if exists fsoc.mf_ProjectMembers_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.mf_ProjectMembers_",yr," ",
"select mf_id ",
",user_id ",
",created_at ",
"from fsoc.presf_mf_",yr," mf ",
"left join github.project_members pm ",
"on pm.repo_id=mf.MF_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("drop table if exists fsoc.pf_ProjectMembers_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.pf_ProjectMembers_",yr," ",
"select mf_id ",
",pf_id ",
",user_id ",
",created_at ",
"from fsoc.presf_pf_",yr," pf ",
"left join github.project_members pm ",
"on pm.repo_id=pf.PF_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.mf_pf_ProjectMembers_indexes$$
create procedure fsoc.mf_pf_ProjectMembers_indexes(in yr int)
begin
set @st=concat("alter table fsoc.mf_ProjectMembers_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.pf_ProjectMembers_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.pf_ProjectMembers_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;

