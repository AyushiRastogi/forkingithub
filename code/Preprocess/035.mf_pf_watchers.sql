delimiter $$
drop procedure if exists fsoc.mf_pf_watchers$$
create procedure fsoc.mf_pf_watchers(in yr int)
begin
set @st=concat("drop table if exists fsoc.mf_watchers_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.mf_watchers_",yr," ",
"select MF_id ",
",user_id ",
",created_at ",
"from fsoc.presf_mf_",yr," sf ",
"left join github.watchers wtch ",
"on wtch.repo_id=sf.MF_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("drop table if exists fsoc.pf_watchers_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.pf_watchers_",yr," ",
"select MF_id ",
",PF_id ",
",user_id ",
",created_at ",
"from fsoc.presf_pf_",yr," sf ",
"left join github.watchers wtch ",
"on wtch.repo_id=sf.PF_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.mf_pf_watchers_indexes$$
create procedure fsoc.mf_pf_watchers_indexes(in yr int)
begin
set @st=concat("alter table fsoc.mf_watchers_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.pf_watchers_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.pf_watchers_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;

