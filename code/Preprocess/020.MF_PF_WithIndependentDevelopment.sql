delimiter $$
drop procedure if exists fsoc.MF_PF_WithIndependentDevelopment$$
create procedure fsoc.MF_PF_WithIndependentDevelopment(in yr int)
begin
set @st=concat("drop table if exists fsoc.MFWithIndependentDevelopment_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.MFWithIndependentDevelopment_",yr," ",
"select distinct MF_id ",
"from fsoc.CommitHistoryOfPF_IndDevPF_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("drop table if exists fsoc.PFWithIndependentDevelopment_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.PFWithIndependentDevelopment_",yr," ",
"select distinct MF_id, PF_id ",
"from fsoc.CommitHistoryOfPF_IndDevPF_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.MF_PF_WithIndependentDevelopment_indexes$$
create procedure fsoc.MF_PF_WithIndependentDevelopment_indexes(in yr int)
begin
set @st=concat("alter table fsoc.MFWithIndependentDevelopment_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("alter table fsoc.PFWithIndependentDevelopment_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("alter table fsoc.PFWithIndependentDevelopment_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
end$$
delimiter ;