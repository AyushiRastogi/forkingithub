delimiter $$
drop procedure if exists fsoc.preForkEndogenousPF$$
create procedure fsoc.preForkEndogenousPF(in yr int)
begin
set @st=concat("drop table if exists fsoc.preForkEndogenousPF_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.preForkEndogenousPF_",yr," ",
"select distinct MF_id ",
",PF_id ",
",NULL as SF_id ",
",user_id ",
",'PF Endogenous' as `type` ",
",id as pull_request_id ",
",merged ",
"from fsoc.preSF_",yr," frk ",
",github.pull_requests pr ",
"where pr.base_repo_id=MF_id ",
"and pr.head_repo_id=PF_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.preForkEndogenousPF_indexes$$
create procedure fsoc.preForkEndogenousPF_indexes(in yr int)
begin
set @st=concat("alter table fsoc.preForkEndogenousPF_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
set @st=concat("alter table fsoc.preForkEndogenousPF_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;
