delimiter $$
drop procedure if exists fsoc.preForkExogenousNotMerged$$
create procedure fsoc.preForkExogenousNotMerged(in yr int)
begin
set @st=concat("drop table if exists fsoc.preForkExogenousNotMerged_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.preForkExogenousNotMerged_",yr," ",
"select exo.* ",
"from fsoc.preForkExogenous_",yr," exo ",
"left join fsoc.preForkMerged_",yr," mrg ",
"on mrg.MF_id=exo.MF_id ",
"where mrg.PF_id!=exo.PF_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.preForkExogenousNotMerged_indexes$$
create procedure fsoc.preForkExogenousNotMerged_indexes(in yr int)
begin
set @st=concat("alter table fsoc.preForkExogenousNotMerged_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
set @st=concat("alter table fsoc.preForkExogenousNotMerged_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
set @st=concat("alter table fsoc.preForkExogenousNotMerged_",yr, " ",
"add index SF_id_",yr,"(SF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;
