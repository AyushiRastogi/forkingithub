delimiter $$
drop procedure if exists fsoc.preSFTimestamped$$
create procedure fsoc.preSFTimestamped(in yr int)
begin
/*
set @st=concat("alter table fsoc.presf_",yr," ",
"drop column MF_createdAt");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.presf_",yr," ",
"drop column PF_createdAt");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.presf_",yr," ",
"drop column SF_createdAt");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;*/

set @st=concat("alter table fsoc.presf_",yr," ",
"add MF_createdAt timestamp null after `MF_id`");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.presf_",yr," ",
"add PF_createdAt timestamp null after `PF_id`");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.presf_",yr," ",
"add SF_createdAt timestamp null after `SF_id`");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

SET SQL_SAFE_UPDATES = 0;
set @st=concat("update fsoc.presf_",yr," sf ",
"left join github.projects proj ",
"on proj.id=sf.MF_id ",
"set MF_createdAt=created_at");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("update fsoc.presf_",yr," sf ",
"left join github.projects proj ",
"on proj.id=sf.PF_id ",
"set PF_createdAt=created_at");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("update fsoc.presf_",yr," sf ",
"left join github.projects proj ",
"on proj.id=sf.SF_id ",
"set SF_createdAt=created_at");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;











