delimiter $$
drop procedure if exists fsoc.preforkPFDormant$$
create procedure fsoc.preforkPFDormant(in yr int)
begin
set @st=concat("drop table if exists fsoc.preforkPFDormant_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.preforkPFDormant_",yr," ",
"select distinct noPR.MF_id ",
",noPR.PF_id ",
",SF_id ",
",user_id ",
",'Dormant' as type ",
",pull_request_id ",
",merged ",
"from fsoc.preforkPFWithNoPR_",yr," noPR ",
"left join fsoc.PFWithIndependentDevelopment_",yr," id ",
"on noPR.MF_id=id.MF_id ",
"where noPR.PF_id!=id.PF_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.preforkPFDormant_indexes$$
create procedure fsoc.preforkPFDormant_indexes(in yr int)
begin
set @st=concat("alter table fsoc.preforkPFDormant_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
set @st=concat("alter table fsoc.preforkPFDormant_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;
