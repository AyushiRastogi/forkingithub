delimiter $$
drop procedure if exists fsoc.CommitHistoryOfPF_IndDevPF$$
create procedure fsoc.CommitHistoryOfPF_IndDevPF(in yr int)
begin
set @st=concat("drop table if exists fsoc.CommitHistoryOfPF_IndDevPF_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.CommitHistoryOfPF_IndDevPF_",yr," ",
"select noPR.MF_id ",
",noPR.PF_id ",
",id commit_id ",
",author_id ",
",committer_id ",
",created_at ",
"from fsoc.preforkPFWithNoPR_",yr," noPR ",
"left join github.commits comm ",
"on comm.project_id=noPR.PF_id ",
"where id is not null");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.CommitHistoryOfPF_IndDevPF_indexes$$
create procedure fsoc.CommitHistoryOfPF_IndDevPF_indexes(in yr int)
begin
set @st=concat("alter table fsoc.CommitHistoryOfPF_IndDevPF_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
set @st=concat("alter table fsoc.CommitHistoryOfPF_IndDevPF_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;
