delimiter $$
drop procedure if exists fsoc.prePF$$
create procedure fsoc.prePF(in yr int)
begin
set @st=concat("drop table if exists fsoc.prePF_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.prePF_",yr," ",
"select MF_id ",
",id as PF_id ",
"from fsoc.pre_MF_",yr," mf ",
"left join github.projects proj ",
"on proj.forked_from=mf.MF_id ",
"where id is not null");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.prePF_indexes$$
create procedure fsoc.prePF_indexes(in yr int)
begin
set @st=concat("alter table fsoc.prePF_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
set @st=concat("alter table fsoc.prePF_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;










