delimiter $$
drop procedure if exists fsoc.preForkMerged$$
create procedure fsoc.preForkMerged(in yr int)
begin
set @st=concat("drop table if exists fsoc.preForkExogenousMerged_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.preForkExogenousMerged_",yr," ",
"select exo.* ",
"from fsoc.preForkExogenous_",yr," exo ",
"left join fsoc.preForkEndogenousPF_",yr," endo ",
"on endo.MF_id=exo.MF_id ",
"where endo.PF_id=exo.PF_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("drop table if exists fsoc.preForkPFIntraMerged_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.preForkPFIntraMerged_",yr," ",
"select exo.* ",
"from fsoc.preForkPFIntra_",yr," exo ",
"left join fsoc.preForkEndogenousPF_",yr," endo ",
"on endo.MF_id=exo.MF_id ",
"where endo.PF_id=exo.PF_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("drop table if exists fsoc.preForkMerged_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.preForkMerged_",yr," ",
"select cast(MF_id as unsigned) MF_id ",
",cast(PF_id as unsigned) PF_id ",
",cast(SF_id as unsigned) SF_id ",
",cast(user_id as unsigned) user_id ",
",'Merged' as type ",
",cast(pull_request_id as unsigned) pull_request_id ",
",cast(merged as unsigned) merged ",
"from fsoc.preForkExogenousMerged_",yr," ",
"union ",
"select cast(MF_id as unsigned) MF_id ",
",cast(PF_id as unsigned) PF_id ",
",cast(SF_id as unsigned) SF_id ",
",cast(user_id as unsigned) user_id ",
",'Merged' as type ",
",cast(pull_request_id as unsigned) pull_request_id ",
",cast(merged as unsigned) merged ",
"from fsoc.preForkPFIntraMerged_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.preForkMerged_indexes$$
create procedure fsoc.preForkMerged_indexes(in yr int)
begin
set @st=concat("alter table fsoc.preForkMerged_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
set @st=concat("alter table fsoc.preForkMerged_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;
