delimiter $$
drop procedure if exists fsoc.mf_pf_issueEvents$$
create procedure fsoc.mf_pf_issueEvents(in yr int)
begin
set @st=concat("drop table if exists fsoc.mf_issueEvents_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.mf_issueEvents_",yr," ",
"select mf_id ",
",iss.issue_id ",
",actor_id ",
",action ",
",ie.created_at ",
"from fsoc.mf_issues_",yr," iss ",
"left join github.issue_events ie ",
"on ie.issue_id=iss.issue_id ",
"where action in ('subscribed','unsubscribed')");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("drop table if exists fsoc.pf_issueEvents_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.pf_issueEvents_",yr," ",
"select mf_id ",
", pf_id ",
",iss.issue_id ",
",actor_id ",
",action ",
",ie.created_at ",
"from fsoc.pf_issues_",yr," iss ",
"left join github.issue_events ie ",
"on ie.issue_id=iss.issue_id ",
"where action in ('subscribed','unsubscribed')");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.mf_pf_issueEvents_indexes$$
create procedure fsoc.mf_pf_issueEvents_indexes(in yr int)
begin
set @st=concat("alter table fsoc.mf_issueEvents_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.pf_issueEvents_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.pf_issueEvents_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;

