drop table if exists fsoc.year;
create table fsoc.year
select 2008 as year
union
select 2009 as year
union
select 2010 as year
union
select 2011 as year
union
select 2012 as year;

drop table if exists fsoc.contribution_year;
create table fsoc.contribution_year
select 2008 as year
union
select 2009 as year
union
select 2010 as year
union
select 2011 as year
union
select 2012 as year
union
select 2013 as year;

drop table if exists fsoc.contribution_month;
create table fsoc.contribution_month 
select 01 as month
union
select 02 as month
union
select 03 as month
union
select 04 as month
union
select 05 as month
union
select 06 as month
union
select 07 as month
union
select 08 as month
union
select 09 as month
union
select 10 as month
union
select 11 as month
union
select 12 as month;

drop table if exists fsoc.contribution_year_month;
create table fsoc.contribution_year_month
select year
,month
from fsoc.contribution_year
,fsoc.contribution_month;



