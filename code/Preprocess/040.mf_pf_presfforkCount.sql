delimiter $$
drop procedure if exists fsoc.mf_pf_presfforkCount$$
create procedure fsoc.mf_pf_presfforkCount(in yr int)
begin
/*
set @st=concat("alter table fsoc.presf_",yr," ",
"drop column mf_forkCount");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;*/


set @st=concat("alter table fsoc.presf_",yr," ",
"add column mf_forkCount varchar(20) null after mf_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

SET SQL_SAFE_UPDATES = 0;

set @st=concat("update fsoc.presf_",yr," sf ",
"left join fsoc.fork_count_",yr," fc ",
"on fc.mf_id=sf.MF_Id ",
"set mf_forkCount=fork_Count");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;


