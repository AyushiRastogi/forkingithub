delimiter $$
drop procedure if exists fsoc.preForkPFWithNoPR$$
create procedure fsoc.preForkPFWithNoPR(in yr int)
begin
set @st=concat("drop table if exists fsoc.preForkPFWithNoPR_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.preForkPFWithNoPR_",yr," ",
"select distinct SF.MF_id ",
",SF.PF_id ",
",NULL as SF_id ",
",NULL as user_id ",
",'PF With No PR' as type ", 
",NULL as pull_request_id ",
",NULL as merged ",
"from fsoc.preforkType_",yr," frk ",
"right join fsoc.preSF_",yr," SF ",
"on SF.MF_id=frk.MF_id ",
"and SF.PF_id!=frk.PF_id ",
"where frk.PF_id is not null");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.preForkPFWithNoPR_indexes$$
create procedure fsoc.preForkPFWithNoPR_indexes(in yr int)
begin
set @st=concat("alter table fsoc.preForkPFWithNoPR_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
set @st=concat("alter table fsoc.preForkPFWithNoPR_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;
