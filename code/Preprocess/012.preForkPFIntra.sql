delimiter $$
drop procedure if exists fsoc.preForkPFIntra$$
create procedure fsoc.preForkPFIntra(in yr int)
begin
set @st=concat("drop table if exists fsoc.preForkPFIntra_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.preForkPFIntra_",yr," ",
"select distinct MF_id ",
",PF_id ",
",NULL as SF_id ",
",user_id ",
",'PF Intra' as `type` ",
",id as pull_request_id ",
",merged ",
"from fsoc.preSF_",yr," frk ",
",github.pull_requests pr ",
"where pr.base_repo_id=PF_id ",
"and pr.head_repo_id=PF_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.preForkPFIntra_indexes$$
create procedure fsoc.preForkPFIntra_indexes(in yr int)
begin
set @st=concat("alter table fsoc.preForkPFIntra_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
set @st=concat("alter table fsoc.preForkPFIntra_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
end$$
delimiter ;
