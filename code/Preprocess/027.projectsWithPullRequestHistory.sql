delimiter $$
drop procedure if exists fsoc.projectsWithPullRequestHistory$$
create procedure fsoc.projectsWithPullRequestHistory(in yr int)
begin
set @st=concat("drop table if exists fsoc.projectsWithPullRequestHistory_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.projectsWithPullRequestHistory_",yr," ",
"select distinct proj.* ",
",created_at ",
",action ",
",actor_id ",
"from fsoc.projectsWithForksOnPR_",yr," proj ",
"left join github.pull_request_history prh ",
"on prh.pull_request_id=proj.pull_request_id ",
"where type!='Dormant'");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.projectsWithPullRequestHistory_indexes$$
create procedure fsoc.projectsWithPullRequestHistory_indexes(in yr int)
begin
set @st=concat("alter table fsoc.projectsWithPullRequestHistory_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;

