delimiter $$
drop procedure if exists fsoc.preSF$$
create procedure fsoc.preSF(in yr int)
begin
set @st=concat("drop table if exists fsoc.preSF_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.preSF_",yr," ",
"select MF_id ",
",PF_id ",
",id as SF_id ",
"from fsoc.prePF_",yr," pf ",
"left join github.projects proj ",
"on proj.forked_from=pf.PF_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.preSF_indexes$$
create procedure fsoc.preSF_indexes(in yr int)
begin
set @st=concat("alter table fsoc.preSF_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
set @st=concat("alter table fsoc.preSF_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
set @st=concat("alter table fsoc.preSF_",yr, " ",
"add index SF_id_",yr,"(SF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;

