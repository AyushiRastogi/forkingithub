delimiter $$
drop procedure if exists fsoc.projectsWithForksOnPR$$
create procedure fsoc.projectsWithForksOnPR(in yr int)
begin
set @st=concat("drop table if exists fsoc.projectsWithForksOnPR_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.projectsWithForksOnPR_",yr," ",
"select distinct ft.* ",
"from fsoc.ForkType_",yr," ft ",
"right join fsoc.MFWithExoIntraForks_",yr," mf ",
"on mf.MF_id=ft.MF_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.projectsWithForksOnPR_indexes$$
create procedure fsoc.projectsWithForksOnPR_indexes(in yr int)
begin
set @st=concat("alter table fsoc.projectsWithForksOnPR_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;
