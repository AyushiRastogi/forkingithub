delimiter $$
drop procedure if exists fsoc.MFCreatedBeforeStartDate$$
create procedure fsoc.MFCreatedBeforeStartDate(in yr int)
begin
set @st=concat("drop table if exists fsoc.MFCreatedBeforeStartDate_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.MFCreatedBeforeStartDate_",yr," ",
"select MF_id ",
",MF_created_at ",
",min(created_at) as MF_min_commit_created_at ",
"from fsoc.pre_MF_",yr," pre_MF ",
"left join github.commits comm ",
"on comm.project_id=pre_MF.MF_id ",
"group by MF_id ",
"having MF_created_at<= MF_min_commit_created_at ");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.MFCreatedBeforeStartDate_indexes$$
create procedure fsoc.MFCreatedBeforeStartDate_indexes(in yr int)
begin
set @st=concat("alter table fsoc.MFCreatedBeforeStartDate_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
end$$
delimiter ;











