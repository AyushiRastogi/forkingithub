delimiter $$
drop procedure if exists fsoc.MFMirrorClone$$
create procedure fsoc.MFMirrorClone(in yr int)
begin
set @st=concat("drop table if exists fsoc.MFMirrorClone_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.MFMirrorClone_",yr," ",
"select MF_id ",
",MF_created_at ",
"from fsoc.MFCreatedBeforeStartDate_",yr," mf ",
"left join github.projects proj ",
"on proj.id=mf.MF_id ",
"where description not like '%mirror%' ",
"and description not like '%clone%'");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.MFMirrorClone_indexes$$
create procedure fsoc.MFMirrorClone_indexes(in yr int)
begin
set @st=concat("alter table fsoc.MFMirrorClone_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
end$$
delimiter ;












