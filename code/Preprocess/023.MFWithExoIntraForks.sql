delimiter $$
drop procedure if exists fsoc.MF_PF_WithExoIntraForks$$
create procedure fsoc.MF_PF_WithExoIntraForks(in yr int)
begin
set @st=concat("drop table if exists fsoc.MFWithExoIntraForks_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.MFWithExoIntraForks_",yr," ",
"select distinct MF_id ",
"from fsoc.preForkType_",yr," ",
"where type ='Exogenous' or type='PF Intra'");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("drop table if exists fsoc.PFWithExoIntraForks_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.PFWithExoIntraForks_",yr," ",
"select distinct MF_id,PF_id ",
"from fsoc.preForkType_",yr," ",
"where type ='Exogenous' or type='PF Intra'");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.MF_PF_WithExoIntraForks_indexes$$
create procedure fsoc.MF_PF_WithExoIntraForks_indexes(in yr int)
begin
set @st=concat("alter table fsoc.MFWithExoIntraForks_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.PFWithExoIntraForks_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;

set @st=concat("alter table fsoc.PFWithExoIntraForks_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;











