delimiter $$
drop procedure if exists fsoc.Outside_MFMirrorClone$$
create procedure fsoc.Outside_MFMirrorClone(in yr int)
begin
set @st=concat("drop table if exists fsoc.Outside_MFMirrorClone_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.Outside_MFMirrorClone_",yr," ",
"select distinct pre.MF_id ",
"from fsoc.pre_mf_",yr," pre ",
"left join fsoc.mfMirrorClone_",yr," mc ",
"on mc.MF_id=pre.MF_id ",
"where mc.MF_id is null");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.Outside_MFMirrorClone_indexes$$
create procedure fsoc.Outside_MFMirrorClone_indexes(in yr int)
begin
set @st=concat("alter table fsoc.Outside_MFMirrorClone_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
end$$
delimiter ;













