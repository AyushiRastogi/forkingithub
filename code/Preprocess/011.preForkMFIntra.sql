delimiter $$
drop procedure if exists fsoc.preForkMFIntra$$
create procedure fsoc.preForkMFIntra(in yr int)
begin
set @st=concat("drop table if exists fsoc.preForkMFIntra_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.preForkMFIntra_",yr," ",
"select distinct MF_id ",
",NULL as PF_id ",
",NULL as SF_id ",
",user_id ",
",'MF Intra' as `type` ",
",id as pull_request_id ",
",merged ",
"from fsoc.preSF_",yr," frk ",
",github.pull_requests pr ",
"where pr.base_repo_id=MF_id ",
"and pr.head_repo_id=MF_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.preForkMFIntra_indexes$$
create procedure fsoc.preForkMFIntra_indexes(in yr int)
begin
set @st=concat("alter table fsoc.preForkMFIntra_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;

