delimiter $$
drop procedure if exists fsoc.mf_pf_presfWatchersCount$$
create procedure fsoc.mf_pf_presfWatchersCount(in yr int)
begin
/*
set @st=concat("alter table fsoc.presf_",yr," ",
"drop column mf_WatchersCount");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.presf_",yr," ",
"drop column pf_WatchersCount");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.presf_",yr," ",
"drop column sf_WatchersCount");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;*/

set @st=concat("alter table fsoc.presf_",yr," ",
"add column mf_WatchersCount varchar(20) null after mf_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.presf_",yr," ",
"add column pf_WatchersCount varchar(20) null after pf_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.presf_",yr," ",
"add column sf_WatchersCount varchar(20) null after sf_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

SET SQL_SAFE_UPDATES = 0;

set @st=concat("update fsoc.presf_",yr," sf ",
"left join fsoc.watchers_count wc ",
"on wc.repo_id=sf.MF_id ",
"set mf_WatchersCount=Watchers_Count");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("update fsoc.presf_",yr," sf ",
"left join fsoc.watchers_count wc ",
"on wc.repo_id=sf.PF_id ",
"set pf_WatchersCount=Watchers_Count");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("update fsoc.presf_",yr," sf ",
"left join fsoc.watchers_count wc ",
"on wc.repo_id=sf.SF_id ",
"set sf_WatchersCount=Watchers_Count");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;


