delimiter $$
drop procedure if exists fsoc.mf_pf_presfDomain$$
create procedure fsoc.mf_pf_presfDomain(in yr int)
begin
/*
set @st=concat("alter table fsoc.presf_",yr," ",
"drop column mf_Domain");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.presf_",yr," ",
"drop column pf_Domain");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.presf_",yr," ",
"drop column sf_Domain");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
*/

set @st=concat("alter table fsoc.presf_",yr," ",
"add column mf_Domain varchar(20) null after mf_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.presf_",yr," ",
"add column pf_Domain varchar(20) null after pf_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.presf_",yr," ",
"add column sf_Domain varchar(20) null after sf_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

SET SQL_SAFE_UPDATES = 0;

set @st=concat("update fsoc.presf_",yr," sf ",
"left join fsoc.projectDomain dom ",
"on dom.id=sf.MF_id ",
"set mf_Domain=Domain");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("update fsoc.presf_",yr," sf ",
"left join fsoc.projectDomain dom ",
"on dom.id=sf.PF_id ",
"set pf_Domain=Domain");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("update fsoc.presf_",yr," sf ",
"left join fsoc.projectDomain dom ",
"on dom.id=sf.SF_id ",
"set sf_Domain=Domain");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;


