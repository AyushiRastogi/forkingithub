delimiter $$
drop procedure if exists fsoc.preForkEndogenousSF$$
create procedure fsoc.preForkEndogenousSF(in yr int)
begin
set @st=concat("drop table if exists fsoc.preForkEndogenousSF_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.preForkEndogenousSF_",yr," ",
"select distinct MF_id ",
",PF_id ",
",SF_id ",
",user_id ",
",'SF Endogenous' as `type` ",
",id as pull_request_id ",
",merged ",
"from fsoc.preSF_",yr," frk ",
",github.pull_requests pr ",
"where pr.base_repo_id=MF_id ",
"and pr.head_repo_id=SF_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.preForkEndogenousSF_indexes$$
create procedure fsoc.preForkEndogenousSF_indexes(in yr int)
begin
set @st=concat("alter table fsoc.preForkEndogenousSF_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
set @st=concat("alter table fsoc.preForkEndogenousSF_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
set @st=concat("alter table fsoc.preForkEndogenousSF_",yr, " ",
"add index SF_id_",yr,"(SF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;