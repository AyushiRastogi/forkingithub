delimiter $$
drop procedure if exists fsoc.ForkType$$
create procedure fsoc.ForkType(in yr int)
begin
set @st=concat("drop table if exists fsoc.ForkType_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.ForkType_",yr," ",
"select * ",
"from fsoc.preForkType_",yr," ",
"union all ",
"select cast(MF_id as unsigned) MF_Id ",
",cast(PF_id as unsigned) PF_id ",
",cast(SF_id as unsigned) SF_id ",
",cast(user_id as unsigned) user_id ",
",cast(type as char(20)) as type ",
",cast(pull_request_id as unsigned) pull_request_id ",
",cast(merged as unsigned) merged ",
"from fsoc.preForkPFDormant_",yr," ");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.ForkType_indexes$$
create procedure fsoc.ForkType_indexes(in yr int)
begin
set @st=concat("alter table fsoc.ForkType_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
set @st=concat("alter table fsoc.ForkType_",yr, " ",
"add index PF_id_",yr,"(PF_id)");
prepare stmt3 from @st;
execute stmt3;
set @st=concat("alter table fsoc.ForkType_",yr, " ",
"add index SF_id_",yr,"(SF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;
