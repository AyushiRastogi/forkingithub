delimiter $$
drop procedure if exists fsoc.preForkType$$
create procedure fsoc.preForkType(in yr int)
begin
set @st=concat("drop table if exists fsoc.preForkType_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;
set @st=concat("create table fsoc.preForkType_",yr," ",
"select cast(MF_id as unsigned) MF_Id ",
",cast(PF_id as unsigned) PF_id ",
",cast(SF_id as unsigned) SF_id ",
",cast(user_id as unsigned) user_id ",
",cast(type as char(20)) as type ",
",cast(pull_request_id as unsigned) pull_request_id ",
",cast(merged as unsigned) merged ",
"from fsoc.preForkEndogenousPF_",yr," ",
"union all ",
"select cast(MF_id as unsigned) MF_Id ",
",cast(PF_id as unsigned) PF_id ",
",cast(SF_id as unsigned) SF_id ",
",cast(user_id as unsigned) user_id ",
",cast(type as char(20)) as type ",
",cast(pull_request_id as unsigned) pull_request_id ",
",cast(merged as unsigned) merged ",
"from fsoc.preForkEndogenousSF_",yr," ",
"union all ",
"select cast(MF_id as unsigned) MF_Id ",
",cast(PF_id as unsigned) PF_id ",
",cast(SF_id as unsigned) SF_id ",
",cast(user_id as unsigned) user_id ",
",cast(type as char(20)) as type ",
",cast(pull_request_id as unsigned) pull_request_id ",
",cast(merged as unsigned) merged ",
"from fsoc.preForkExogenousNotMerged_",yr," ",
"union all ",
"select cast(MF_id as unsigned) MF_Id ",
",cast(PF_id as unsigned) PF_id ",
",cast(SF_id as unsigned) SF_id ",
",cast(user_id as unsigned) user_id ",
",cast(type as char(20)) as type ",
",cast(pull_request_id as unsigned) pull_request_id ",
",cast(merged as unsigned) merged ",
"from fsoc.preForkMFIntra_",yr," ",
"union all ",
"select cast(MF_id as unsigned) MF_Id ",
",cast(PF_id as unsigned) PF_id ",
",cast(SF_id as unsigned) SF_id ",
",cast(user_id as unsigned) user_id ",
",cast(type as char(20)) as type ",
",cast(pull_request_id as unsigned) pull_request_id ",
",cast(merged as unsigned) merged ",
"from fsoc.preForkPFIntraNotMerged_",yr," "
"union all ",
"select cast(MF_id as unsigned) MF_Id ",
",cast(PF_id as unsigned) PF_id ",
",cast(SF_id as unsigned) SF_id ",
",cast(user_id as unsigned) user_id ",
",cast(type as char(20)) as type ",
",cast(pull_request_id as unsigned) pull_request_id ",
",cast(merged as unsigned) merged ",
"from fsoc.preForkMerged_",yr);
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;

delimiter $$
drop procedure if exists fsoc.preForkType_indexes$$
create procedure fsoc.preForkType_indexes(in yr int)
begin
set @st=concat("alter table fsoc.preForkType_",yr, " ",
"add index MF_id_",yr,"(MF_id)");
prepare stmt3 from @st;
execute stmt3;
end$$
delimiter ;
