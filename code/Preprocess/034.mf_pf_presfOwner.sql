delimiter $$
drop procedure if exists fsoc.mf_pf_presfOwner$$
create procedure fsoc.mf_pf_presfOwner(in yr int)
begin
/*
set @st=concat("alter table fsoc.presf_",yr," ",
"drop column mf_Owner");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.presf_",yr," ",
"drop column pf_Owner");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.presf_",yr," ",
"drop column sf_Owner");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;*/

set @st=concat("alter table fsoc.presf_",yr," ",
"add column mf_Owner varchar(20) null after mf_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.presf_",yr," ",
"add column pf_Owner varchar(20) null after pf_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("alter table fsoc.presf_",yr," ",
"add column sf_Owner varchar(20) null after sf_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

SET SQL_SAFE_UPDATES = 0;

set @st=concat("update fsoc.presf_",yr," sf ",
"left join github.projects proj ",
"on sf.mf_id=proj.id ",
"set mf_Owner=owner_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("update fsoc.presf_",yr," sf ",
"left join github.projects proj ",
"on sf.pf_id=proj.id ",
"set pf_Owner=owner_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

set @st=concat("update fsoc.presf_",yr," sf ",
"left join github.projects proj ",
"on sf.sf_id=proj.id ",
"set sf_Owner=owner_id");
prepare stmt3 from @st;
execute stmt3;
deallocate prepare stmt3;

end$$
delimiter ;


